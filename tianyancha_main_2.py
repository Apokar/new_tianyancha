# -*- coding: utf-8 -*-
# @Time         : 2017/12/12 15:08
# @Author       : Huaiz
# @Email        : Apokar@163.com
# @File         : tianyancha_main.py
# @Software     : PyCharm Community Edition
# @PROJECT_NAME : new_tianyancha
import json
import sys
import urllib
import traceback

reload(sys)
sys.setdefaultencoding('utf8')

import urllib3
import lxml
import db_config

urllib3.disable_warnings()
import os
import re
import time
import requests
import threading
import random
import datetime
import MySQLdb
from bs4 import BeautifulSoup
from requests.packages.urllib3.exceptions import InsecureRequestWarning

# 禁用安全请求警告
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

static_js_code = """
var ne = "2633141825201321121345332721524273528936811101916293117022304236|1831735156281312241132340102520529171363214283321272634162219930|2332353860219720155312141629130102234183691124281413251227261733|2592811262018293062732141927100364232411333831161535317211222534|9715232833130331019112512913172124126035262343627321642220185148|3316362031032192529235212215274341412306269813312817111724201835|3293412148301016132183119242311021281920736172527353261533526224|3236623313013201625221912357142415851018341117262721294332103928|2619332514511302724163415617234183291312001227928218353622321031|3111952725113022716818421512203433241091723133635282932601432216";
var base64chars = "abcdefghijklmnopqrstuvwxyz1234567890-~!";
var _0x4fec = "f9D1x1Z2o1U2f5A1a1P1i7R1u2S1m1F1,o2A1x2F1u5~j1Y2z3!p2~r3G2m8S1c1,i3E5o1~d2!y2H1e2F1b6`g4v7,p1`t7D3x5#w2~l2Z1v4Y1k4M1n1,C2e3P1r7!s6U2n2~p5X1e3#,g4`b6W1x4R1r4#!u5!#D1f2,!z4U1f4`f2R2o3!l4I1v6F1h2F1x2!,b2~u9h2K1l3X2y9#B4t1,t5H1s7D1o2#p2#z1Q3v2`j6,r1#u5#f1Z2w7!r7#j3S1";
rs_decode = function(e) {
    return ne.split("|")[e]
};
    var r = t+"";
    r = r.length > 1 ? r[1] : r;
    for (var i = rs_decode(r), o = _0x4fec.split(",")[r], a = [], s = 0, u = 0; u < o.length; u++) {
        if ("`" != o[u] && "!" != o[u] && "~" != o[u] || (a.push(i.substring(s, s + 1)), s++), "#" == o[u] && (a.push(i.substring(s, s + 1)), a.push(i.substring(s + 1, s + 3)), a.push(i.substring(s + 3, s + 4)), s += 4), o.charCodeAt(u) > 96 && o.charCodeAt(u) < 123) for (var l = o[u + 1], c = 0; c < l; c++) a.push(i.substring(s, s + 2)),
        s += 2;
        if (o.charCodeAt(u) > 64 && o.charCodeAt(u) < 91) for (var l = o[u + 1], c = 0; c < l; c++) a.push(i.substring(s, s + 1)),
        s++
    }
    rsid = a;
for (var chars = "",  i = 0; i < rsid.length; i++) chars += base64chars[rsid[i]];
for (var fxck = wtf.split(","), fxckStr = "", i = 0; i < fxck.length; i++) fxckStr += chars[fxck[i]];
var utm = fxckStr;
console.log("{\\"utm\\":\\""+utm+"\\",\\"ssuid\\":\\""+Math.round(2147483647 * Math.random()) * (new Date).getUTCMilliseconds() % 1e10+"\\"}")
phantom.exit();
"""


def execCmd(cmd):
    text = os.popen(cmd).read()
    return (text)


# 正 则
def re_findall(pattern, html):
    if re.findall(pattern, html, re.S):
        return re.findall(pattern, html, re.S)
    else:
        return 'NNNNNNNN'


def detag(html):
    detag = re.subn('<[^>]*>', ' ', html)[0]
    detag = re.subn('\\\\u\w{4}', ' ', detag)[0]
    detag = detag.replace('{', '')
    detag = detag.replace('}', '')
    detag = detag.replace('"', '')
    detag = detag.replace(' ', '')
    detag = detag.replace('\n', '')
    detag = detag.replace('\t', '')
    detag = detag.replace('&amp;', '|')

    return detag


def get_proxy():
    proxy_list = list(set(urllib.urlopen(
        'http://60.205.92.109/api.do?name=3E30E00CFEDCD468E6862270F5E728AF&status=1&type=static').read().split('\n')[
                          :-1]))
    index = random.randint(0, len(proxy_list) - 1)
    current_proxy = proxy_list[index]
    print "NEW PROXY:\t%s" % current_proxy + ' --- ' + str(datetime.datetime.now())
    proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }
    return proxies


def get_page(url):
    headers = {
        'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
        'Host': 'www.tianyancha.com',
        'Referer': 'https://www.tianyancha.com/',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
    }
    proxies = get_proxy()
    html = requests.get(url, proxies=proxies, timeout=10, headers=headers)
    return html


###############################################


def jingpin(url, cursor, conn):
    if url not in old_jingpin:
        print u'获取竞品信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                # print html.text

                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-companyJingpin'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-companyJingpin > span')[0].text
                        if int(num) % 10 == 0:
                            all_page_no = int(num) / 10
                        else:
                            all_page_no = int(num) / 10 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('jingpin', i, 10, corp_name)
                            res = soup2.select('tr')

                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source
                                if source.find('href') >= 0:
                                    icon = re_findall('src="(.*?)"', source)[0]
                                    name = re_findall('img alt="(.*?)"', source)[0]
                                    jingpin_ID = re_findall('href="https://www.tianyancha.com/company/(.*?)"', source)[
                                        0]

                                    location = re_findall('span class=".*?">(.*?)</span>', source)[0]
                                    current_round = re_findall('span class=".*?">(.*?)</span>', source)[1]
                                    industry = re_findall('span class=".*?">(.*?)</span>', source)[2]
                                    business = re_findall('span class=".*?">(.*?)</span>', source)[3]
                                    create_time = re_findall('span class=".*?">(.*?)</span>', source)[4]
                                    valuation = re_findall('span class=".*?">(.*?)</span>', source)[5]
                                else:
                                    icon = re_findall('src="(.*?)"', source)[0]
                                    name = re_findall('img alt="(.*?)"', source)[0]
                                    jingpin_ID = u'not found'
                                    location = re_findall('span class=".*?">(.*?)</span>', source)[1]
                                    current_round = re_findall('span class=".*?">(.*?)</span>', source)[2]
                                    industry = re_findall('span class=".*?">(.*?)</span>', source)[3]
                                    business = re_findall('span class=".*?">(.*?)</span>', source)[4]
                                    create_time = re_findall('span class=".*?">(.*?)</span>', source)[5]
                                    valuation = re_findall('span class=".*?">(.*?)</span>', source)[6]
                                print icon
                                print name
                                print jingpin_ID
                                print location
                                print current_round
                                print industry
                                print business
                                print create_time
                                print valuation
                                print u'插入成功 ok !!! @__ ' + str(datetime.datetime.now())
                                cursor.execute(
                                    'insert into tyc_jingpin_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (
                                        corp_id,
                                        corp_name,
                                        url,
                                        icon.decode('utf-8'),
                                        name.decode('utf-8'),
                                        jingpin_ID,
                                        location.decode('utf-8'),
                                        current_round.decode('utf-8'),
                                        industry.decode('utf-8'),
                                        business.decode('utf-8'),
                                        create_time.decode('utf-8'),
                                        valuation.decode('utf-8'),
                                        str(datetime.datetime.now()),
                                        str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 竞品信息插入成功  ' + str(datetime.datetime.now())
                    break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def staff(url, cursor, conn):
    if url not in old_staff:
        print u'爬取主要人员信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-staffCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-staffCount > span')[0].text
                        if int(num) % 20 == 0:
                            all_page_no = int(num) / 20
                        else:
                            all_page_no = int(num) / 20 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('staff', i, 20, corp_id)
                            # print soup2
                            res = soup2.select('div.staffinfo-module-container')
                            for x in range(len(res)):
                                source = str(res[x])
                                print source
                                position = re_findall('f9f9f9;"><span>(.*?)</span></div>', source)[0]
                                print detag(position)
                                name = re_findall('target="_blank">(.*?)</a><div class="in-block', source)[0]
                                print name
                                ID = re_findall('"企业详情-主要人员" href="(.*?)"', source)[0]
                                print ID
                                cursor.execute(
                                    'insert into tyc_staff_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     detag(position),
                                     name,
                                     ID,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 主要人员信息插入成功  ' + str(datetime.datetime.now())

                break

            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def shareholder(url, cursor, conn):
    if url not in old_shareholder:
        print u'爬取股东信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]
        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-holderCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-holderCount > span')[0].text
                        if int(num) % 10 == 0:
                            all_page_no = int(num) / 10
                        else:
                            all_page_no = int(num) / 10 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('holder', i, 20, corp_id)
                            # print soup2
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                shareholder = re.findall('title="(.*?)"', source)[0]
                                ratio = re.findall('<span class="c-money-y">(.*?)</span>', source)[0]
                                value = re.findall('<span class="">(.*?)</span>', source)[0]
                                print shareholder
                                print ratio
                                print value
                                cursor.execute(
                                    'insert into tyc_shareholder_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,
                                     shareholder,
                                     ratio,
                                     value,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 股东信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def out_invest(url, cursor, conn):
    if url not in old_out_investment:
        print u'爬取对外投资信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-inverst'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-inverstCount > span')[0].text
                        if int(num) % 20 == 0:
                            all_page_no = int(num) / 20
                        else:
                            all_page_no = int(num) / 20 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('invest', i, 20, corp_id)
                            res = soup2.select('tr')
                            for x in range(len(res)):
                                source = str(res[x])
                                print source
                                invested_person = re_findall('title="(.*?)"', source)
                                span_part = re_findall('<span class=".*?">(.*?)</span>', source)
                                print span_part[0]  # 被投资企业名称
                                print invested_person[0]  # 被投资法定代表人
                                print span_part[2]  # 注册资本
                                print span_part[3]  # 投资数额
                                print span_part[4]  # 投资占比
                                print span_part[5]  # 注册时间
                                print span_part[6]  # 状态
                                print '--------------------------'
                                cursor.execute(
                                    'insert tyc_out_investment_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     span_part[0].decode('utf-8'),
                                     invested_person[0].decode('utf-8'),
                                     span_part[2].decode('utf-8'),
                                     span_part[3].decode('utf-8'),
                                     span_part[4].decode('utf-8'),
                                     span_part[5].decode('utf-8'),
                                     span_part[6].decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 对外投资信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:

                if str(e).find('HTTPSConnectionPool') >= 0:

                    print 'HTTPSConnectionPool , retry '

                    continue

                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:

                    return 'MySql error 2006 ,restart'

                elif str(e).find('2003') >= 0:

                    return 'MySql error 2003 ,restart'

                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue

                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def change_info(url, cursor, conn):
    if url not in old_change_record:
        print u'爬取变更记录信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-changeCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-changeCount > span')[0].text
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('changeinfo', i, 20, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                part_one = re_findall('<div>(.*?)</div>', source)
                                part_two = re_findall('<div class="textJustFy changeHoverText">(.*?)</div>', source)
                                change_time = part_one[0]
                                change_projects = part_one[1]
                                before_change = detag(part_two[0])
                                after_change = detag(part_two[1])
                                print change_time.decode('utf-8')
                                print change_projects.decode('utf-8')
                                print before_change.decode('utf-8')
                                print after_change.decode('utf-8')
                                cursor.execute(
                                    'insert into tyc_change_record_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,
                                     change_time.decode('utf-8'),
                                     change_projects.decode('utf-8'),
                                     detag(before_change).decode('utf-8'),
                                     detag(after_change).decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 变更信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def product_info(url, cursor, conn):
    if url not in old_product:
        print u'爬取产品信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-productinfo'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-productinfo > span')[0].text
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('product', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                image = re_findall('src="(.*?)"', source)
                                part_one = re_findall('<span>(.*?)</span>', source)
                                detail_info = re_findall('<td><script type="text/html">(.*?)</script>', source)
                                print image[0]
                                print part_one[0]
                                print part_one[1]
                                print part_one[2]
                                print part_one[3]
                                print detail_info[0]
                                print '--------------------'
                                cursor.execute(
                                    'insert into tyc_product_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     image[0],
                                     part_one[0].decode('utf-8'),
                                     part_one[1].decode('utf-8'),
                                     part_one[2].decode('utf-8'),
                                     part_one[3].decode('utf-8'),
                                     detag(detail_info[0]).decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10]
                                     )
                                )
                                conn.commit()
                        print corp_name + u' 产品信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def rongziHistory(url, cursor, conn):
    if url not in old_rongziCompany:
        print u'获取融资历史  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                # print html.text

                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-companyRongzi'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-companyRongzi > span')[0].text
                        if int(num) % 10 == 0:
                            all_page_no = int(num) / 10
                        else:
                            all_page_no = int(num) / 10 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('rongzi', i, 10, corp_name)
                            res = soup2.select('tr')

                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source
                                all_1 = re_findall('<td><span class="text-dark-color .*?">(.*?)</span></td>', source)
                                print all_1[0]
                                print all_1[1]
                                print all_1[2]
                                print all_1[3]
                                print all_1[4]

                                asd = re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)
                                print detag(asd[0])
                                print detag(asd[1])
                                print u'插入成功 ok !!! @__ ' + str(datetime.datetime.now())
                                cursor.execute(
                                    'insert into tyc_rongziCompany_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (
                                        corp_id,
                                        corp_name,
                                        url,

                                        all_1[0].decode('utf-8'),
                                        all_1[1].decode('utf-8'),
                                        all_1[2].decode('utf-8'),
                                        all_1[3].decode('utf-8'),
                                        all_1[4].decode('utf-8'),
                                        detag(asd[0]).decode('utf-8'),
                                        detag(asd[1]).decode('utf-8'),

                                        str(datetime.datetime.now()),
                                        str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 融资历史信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:

                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def wechat_info(url, cursor, conn):
    if url not in old_wechat:
        print u'爬取微信公众号信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-weChatCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-weChatCount > span')[0].text
                        all_page_no = int(num) / 10 + 1
                        last_page_no = int(num) % 10
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('wechat', i, 10, corp_id)
                            if i < int(all_page_no):

                                for n in range(1, 11):
                                    wechat_icon = soup2.select(
                                        'body > div:nth-of-type(1) > div:nth-of-type(' + str(
                                            n) + ') > div.in-block.vertical-top.wechatImg > img')[0]['src']

                                    wechat_name = soup2.select(
                                        'body > div:nth-of-type(1) > div:nth-of-type(' + str(
                                            n) + ') > div.in-block.vertical-top.itemRight > div:nth-of-type(1)')[0].text

                                    wechat_num = soup2.select(
                                        'body > div:nth-of-type(1) > div:nth-of-type(' + str(
                                            n) + ') > div.in-block.vertical-top.itemRight > div:nth-of-type(2) > span:nth-of-type(2)')[
                                        0].text

                                    wechat_introduce = soup2.select(
                                        'body > div:nth-of-type(1) > div:nth-of-type(' + str(
                                            n) + ') > div.in-block.vertical-top.itemRight > div:nth-of-type(3) > span.overflow-width.in-block.vertical-top')[
                                        0].text

                                    QR_code = soup2.select(
                                        'body > div:nth-of-type(1) > div:nth-of-type(' + str(
                                            n) + ') > div.in-block.vertical-top.itemRight > div:nth-of-type(2) > div > div > img')[
                                        0][
                                        'src']

                                    cursor.execute(
                                        'insert into tyc_wechat_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                        (url,
                                         corp_name,
                                         corp_id,
                                         wechat_icon,
                                         wechat_name,
                                         wechat_num,
                                         wechat_introduce,
                                         QR_code,
                                         str(datetime.datetime.now()),
                                         str(datetime.datetime.now())[:10])
                                    )
                                    conn.commit()



                            else:
                                for n in range(1, int(last_page_no) + 1):
                                    wechat_icon = soup2.select(
                                        'body > div:nth-of-type(1) > div:nth-of-type(' + str(
                                            n) + ') > div.in-block.vertical-top.wechatImg > img')[0]['src']

                                    wechat_name = soup2.select(
                                        'body > div:nth-of-type(1) > div:nth-of-type(' + str(
                                            n) + ') > div.in-block.vertical-top.itemRight > div:nth-of-type(1)')[0].text

                                    wechat_num = soup2.select(
                                        'body > div:nth-of-type(1) > div:nth-of-type(' + str(
                                            n) + ') > div.in-block.vertical-top.itemRight > div:nth-of-type(2) > span:nth-of-type(2)')[
                                        0].text

                                    wechat_introduce = soup2.select(
                                        'body > div:nth-of-type(1) > div:nth-of-type(' + str(
                                            n) + ') > div.in-block.vertical-top.itemRight > div:nth-of-type(3) > span.overflow-width.in-block.vertical-top')[
                                        0].text

                                    QR_code = soup2.select(
                                        'body > div:nth-of-type(1) > div:nth-of-type(' + str(
                                            n) + ') > div.in-block.vertical-top.itemRight > div:nth-of-type(2) > div > div > img')[
                                        0][
                                        'src']

                                    cursor.execute(
                                        'insert into tyc_wechat_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                        (url,
                                         corp_name,
                                         corp_id,
                                         wechat_icon,
                                         wechat_name,
                                         wechat_num,
                                         wechat_introduce,
                                         QR_code,
                                         str(datetime.datetime.now()),
                                         str(datetime.datetime.now())[:10])
                                    )
                                    conn.commit()
                        print corp_name + u' 微信公众号信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def website_record(url, cursor, conn):
    if url not in old_webrecord:
        print u'爬取网站备案信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-icpCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-icpCount > span')[0].text
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('icp', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                part_one = re_findall('<td><span>(.*?)</span></td>', source)
                                part_two = re_findall('target="_blank">(.*?)</a></td>', source)
                                check_time = part_one[0]
                                website_name = part_one[1]
                                homepage = part_two[0]
                                domain = re_findall('<td>(.*?)</td>', source)[3]
                                record_number = part_one[2]
                                state = part_one[3]
                                unit_character = part_one[4]
                                print part_one
                                print part_two
                                print check_time
                                print website_name
                                print homepage
                                print domain
                                print record_number
                                print state
                                print unit_character
                                print '--------------------------'
                                cursor.execute(
                                    'insert into tyc_webrecord_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,
                                     check_time.decode('utf-8'),
                                     website_name.decode('utf-8'),
                                     homepage.decode('utf-8'),
                                     domain.decode('utf-8'),
                                     record_number.decode('utf-8'),
                                     state.decode('utf-8'),
                                     unit_character.decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 微信公众号信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def lawsuit(url, cursor, conn):
    if url not in old_lawsuit:
        print u'爬取法律诉讼信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-lawsuitCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-lawsuitCount > span')[0].text
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('lawsuit', i, 5, corp_name)

                            # print soup2
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source

                                date = re_findall('<span class=".*?">(.*?)</span>', source)[0]
                                Judgment_document_url = re_findall('href="(.*?)" href-new-event', source)[0]
                                Judgment_document_name = re_findall('target="_blank">(.*?)</a>', source)[0]
                                cause = re_findall('<span class=".*?">(.*?)</span>', source)[1]
                                identity = detag(re_findall('<div class="text-dark-color">(.*?)</div>', source)[0])
                                docket_number = re_findall('<span class=".*?">(.*?)</span>', source)[2]
                                cursor.execute(
                                    'insert into tyc_lawsuit_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     date.decode('utf-8'),
                                     Judgment_document_name.decode('utf-8'),
                                     Judgment_document_url.decode('utf-8'),
                                     cause.decode('utf-8'),
                                     identity.decode('utf-8'),
                                     docket_number.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10]
                                     )
                                )
                                conn.commit()
                        print corp_name + u' 法律诉讼信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def court(url, cursor, conn):
    if url not in old_court:
        print u'爬取法院公告信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-courtCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-courtCount > span')[0].text
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('court', i, 5, corp_name)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                date = re_findall('"publishdate":"(.*?)"', source)[0]
                                appellant = re_findall('"party1":"(.*?)"', source)[0]
                                defendant = re_findall('"party2":"(.*?)"', source)[0]
                                type = re_findall('"bltntypename":"(.*?)"', source)[0]
                                court = re_findall('"courtcode":"(.*?)"', source)[0]
                                detail = re_findall('"content":"(.*?)"', source)[0]
                                cursor.execute(
                                    'insert into tyc_court_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,
                                     date.decode('utf-8'),
                                     appellant.decode('utf-8'),
                                     defendant.decode('utf-8'),
                                     type.decode('utf-8'),
                                     court.decode('utf-8'),
                                     detail.decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10]
                                     )
                                )
                                conn.commit()
                        print corp_name + u' 法院公告信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def zhixing(url, cursor, conn):
    if url not in old_zhixing:
        print u'爬取执行信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-zhixing'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-zhixing > span')[0].text
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('zhixing', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                part = re_findall('<span class=".*?">(.*?)</span>', source)
                                print part[0]
                                print part[1]
                                print part[2]
                                print part[3]
                                cursor.execute(
                                    'insert into tyc_zhixing_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,
                                     part[0].decode('utf-8'),
                                     part[1].decode('utf-8'),
                                     part[2].decode('utf-8'),
                                     part[3].decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10]
                                     )
                                )
                                conn.commit()
                        print corp_name + u' 执行信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def announcement(url, cursor, conn):
    if url not in old_announcement:
        print u'爬取开庭公告信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    # print html.text
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-announcementCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-announcementCount > span')[0].text

                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('announcementcourt', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                date = re_findall('<td>(.*?)</td>', source)[0]
                                type = re_findall('<span class="text-dark-color">(.*?)</span>', source)[0]
                                appellant = re_findall('"name":"(.*?)",', source)[0]
                                defendant = re_findall('"name":"(.*?)",', source)[1]
                                case_no = re_findall('"caseNo":"(.*?)",', source)[0]

                                cursor.execute(
                                    'insert into tyc_announcement_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,
                                     date.decode('utf-8'),
                                     type.decode('utf-8'),
                                     appellant.decode('utf-8'),
                                     defendant.decode('utf-8'),
                                     case_no.decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 开庭公告信息插入成功  ' + str(datetime.datetime.now())

                        break
                    else:
                        print "no announcement "
                        break
                break

            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def punish(url, cursor, conn):
    if url not in old_punish:
        print u'爬取惩罚信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-punishment'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-punishment > span')[0].text

                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('punish', i, 5, corp_name)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source

                                date = re_findall('<span class=".*?">(.*?)</span>', source)[0]
                                document_no = re_findall('<span class=".*?">(.*?)</span>', source)[1]
                                type = re_findall('<span class=".*?">(.*?)</span>', source)[2]
                                decision_organ = re_findall('<div class=".*?">(.*?)</div>', source)[0]

                                print date
                                print document_no
                                print type
                                print decision_organ
                                cursor.execute(
                                    'insert into tyc_punish_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,
                                     date.decode('utf-8'),
                                     document_no.decode('utf-8'),
                                     type.decode('utf-8'),
                                     decision_organ.decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 惩罚信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue

                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def recruit(url, cursor, conn):
    if url not in old_recruit:
        print u'爬取招聘信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name

                    if html.text.__contains__('nav-main-recruitCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-recruitCount > span')[0].text
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('recruit', i, 10, corp_name)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source
                                date = re_findall('<span.*?>(.*?)</span>', source)[0]
                                job = re_findall('<span.*?>(.*?)</span>', source)[1]
                                exper = re_findall('<span.*?>(.*?)</span>', source)[2]
                                num = re_findall('<span.*?>(.*?)</span>', source)[3]
                                location = re_findall('<span.*?>(.*?)</span>', source)[4]
                                salary = re_findall('"oriSalary":"(.*?)"', source)[0]
                                detail = re_findall('"description":"(.*?)"', source)[0]
                                fromWeb = re_findall('"source":"(.*?)"', source)[0]
                                cursor.execute(
                                    'insert into tyc_recruit_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     date.decode('utf-8'),
                                     job.decode('utf-8'),
                                     exper.decode('utf-8'),
                                     num.decode('utf-8'),
                                     location.decode('utf-8'),
                                     salary.decode('utf-8'),
                                     detag(detail.decode('utf-8')),
                                     fromWeb.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 招聘信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def patent(url, cursor, conn):
    if url not in old_patent:
        print u'爬取专利信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-patentCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-patentCount > span')[0].text
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('patent', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                date = re_findall('<span.*?>(.*?)</span>', source)[0]
                                name = re_findall('<span.*?>(.*?)</span>', source)[1]
                                application_num = re_findall('<span.*?>(.*?)</span>', source)[2]
                                application_for_pubnum = re_findall('<span.*?>(.*?)</span>', source)[3]

                                print date
                                print name
                                print application_num
                                print application_for_pubnum
                                cursor.execute(
                                    'insert into tyc_patent_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     date.decode('utf-8'),
                                     name.decode('utf-8'),
                                     application_num.decode('utf-8'),
                                     application_for_pubnum.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 专利信息插入成功  ' + str(datetime.datetime.now())

                break

            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def copyR(url, cursor, conn):
    if url not in old_copyRcount:
        print u'获取软件著作权部分  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-cpoyRCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-cpoyRCount > span')[0].text
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('copyright', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                obj = re_findall('<span>(.*?)</span>', source)
                                date = obj[0]
                                name = obj[1]
                                brief_name = obj[2]
                                registration_no = obj[3]
                                classification_no = obj[4]
                                version_no = obj[5]

                                print date
                                print name
                                print brief_name
                                print registration_no
                                print classification_no
                                print version_no
                                cursor.execute(
                                    'insert into tyc_copyRcount_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s" )' %
                                    (url,
                                     corp_name,
                                     corp_id,
                                     date.decode('utf-8'),
                                     name.decode('utf-8'),
                                     brief_name.decode('utf-8'),
                                     registration_no.decode('utf-8'),
                                     classification_no.decode('utf-8'),
                                     version_no.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 软件著作权信息  插入成功  ' + str(datetime.datetime.now())

                break

            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def copyrightWorks(url, cursor, conn):
    if url not in old_copyRworks:
        print u'获取作品著作权部分  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name

                    if html.text.__contains__('nav-main-copyrightWorks'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-copyrightWorks > span')[0].text
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('copyrightWorks', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                obj = re_findall('<span>(.*?)</span>', source)
                                name = obj[0]
                                register_no = obj[1]
                                category = obj[2]
                                finish_date = obj[3]
                                register_date = obj[4]
                                publish_date = obj[5]

                                print name
                                print register_no
                                print category
                                print finish_date
                                print register_date
                                print publish_date
                                cursor.execute(
                                    'insert into tyc_copyRworks_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     name.decode('utf-8'),
                                     register_no.decode('utf-8'),
                                     category.decode('utf-8'),
                                     finish_date.decode('utf-8'),
                                     register_date.decode('utf-8'),
                                     publish_date.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])

                                )
                                conn.commit()
                        print corp_name + u' 作品著作权信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def firmProduct(url, cursor, conn):
    if url not in old_companyProduct:
        print u'获取企业业务部分  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-companyProduct'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-companyProduct > span')[0].text
                        if int(num) % 15 == 0:
                            all_page_no = int(num) / 15
                        else:
                            all_page_no = int(num) / 15 + 1

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('firmProduct', i, 15, corp_name)
                            res = soup2.select('div[class="product-item"]')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                name = re_findall('img alt="(.*?)"', source)[0]
                                img = re_findall('src="(.*?)"', source)[0]
                                category = detag(re_findall('<div class=".*?">(.*?)</div>', source)[2])
                                brief = detag(re_findall('<div class=".*?">(.*?)</div>', source)[3])

                                print name
                                print img
                                print category
                                print brief
                                cursor.execute(
                                    'insert into tyc_companyProduct_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     name.decode('utf-8'),
                                     img.decode('utf-8'),
                                     category.decode('utf-8'),
                                     brief.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 企业业务信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def investment_info(url, cursor, conn):
    if url not in old_investment:
        print u'获取投资事件部分  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-jigouTzanli'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-jigouTzanli > span')[0].text
                        if int(num) % 10 == 0:
                            all_page_no = int(num) / 10
                        else:
                            all_page_no = int(num) / 10 + 1
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('touzi', i, 10, corp_name)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                date = re_findall('<span class=".*?">(.*?)</span>', source)[0]
                                round = re_findall('<span class=".*?">(.*?)</span>', source)[1]
                                amount = re_findall('<span class=".*?">(.*?)</span>', source)[2]
                                investor = detag(re_findall('<div>(.*?)<img alt', source)[0])
                                product_name = re_findall('img alt="(.*?)"', source)[0]
                                product_icon = re_findall('src="(.*?)"', source)[0]
                                location = re_findall('<span class=".*?">(.*?)</span>', source)[3]
                                industry = re_findall('<span class=".*?">(.*?)</span>', source)[4]
                                business = re_findall('<span class=".*?">(.*?)</span>', source)[5]

                                print date
                                print round
                                print amount
                                print investor
                                print product_name
                                print product_icon
                                print location
                                print industry
                                print business
                                cursor.execute(
                                    'insert into tyc_investment_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     date.decode('utf-8'),
                                     round.decode('utf-8'),
                                     amount.decode('utf-8'),
                                     investor.decode('utf-8'),
                                     product_name.decode('utf-8'),
                                     product_icon.decode('utf-8'),
                                     location.decode('utf-8'),
                                     industry.decode('utf-8'),
                                     business.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()

                        print corp_name + u' 投资事件部分信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue

                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()

                    break


def dishonest(url, cursor, conn):
    if url not in old_dishonest:
        print u'爬取失信人信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-dishonest'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-dishonest > span')[0].text
                        for i in range(1, int(num) + 1):
                            date = soup.select(
                                '#web-content > div > div > div.container.company_container > div > div.col-9.company-main.pl0.pr10.company_new_2017 > div > div.pl30.pr30.pt25 > div:nth-of-type(14) > div:nth-of-type(2) > div > table > tbody > tr:nth-of-type(' + str(
                                    i) + ') > td:nth-of-type(1) > span')[0].text
                            case_no = soup.select(
                                '#web-content > div > div > div.container.company_container > div > div.col-9.company-main.pl0.pr10.company_new_2017 > div > div.pl30.pr30.pt25 > div:nth-of-type(14) > div:nth-of-type(2) > div > table > tbody > tr:nth-of-type(' + str(
                                    i) + ') > td:nth-of-type(2) > span')[0].text
                            court = soup.select(
                                '#web-content > div > div > div.container.company_container > div > div.col-9.company-main.pl0.pr10.company_new_2017 > div > div.pl30.pr30.pt25 > div:nth-of-type(14) > div:nth-of-type(2) > div > table > tbody > tr:nth-of-type(' + str(
                                    i) + ') > td:nth-of-type(3) > span')[0].text
                            state = soup.select(
                                '#web-content > div > div > div.container.company_container > div > div.col-9.company-main.pl0.pr10.company_new_2017 > div > div.pl30.pr30.pt25 > div:nth-of-type(14) > div:nth-of-type(2) > div > table > tbody > tr:nth-of-type(' + str(
                                    i) + ') > td:nth-of-type(4) > span')[0].text
                            reference = soup.select(
                                '#web-content > div > div > div.container.company_container > div > div.col-9.company-main.pl0.pr10.company_new_2017 > div > div.pl30.pr30.pt25 > div:nth-of-type(14) > div:nth-of-type(2) > div > table > tbody > tr:nth-of-type(' + str(
                                    i) + ') > td:nth-of-type(5) > span')[0].text
                            print date
                            print case_no
                            print court
                            print state
                            print reference
                            cursor.execute(
                                'insert into tyc_dishonest_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' % (
                                    url,
                                    corp_name,
                                    corp_id,
                                    date,
                                    case_no,
                                    court,
                                    state,
                                    reference,
                                    str(datetime.datetime.now()),
                                    str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                        print corp_name + u' 失信人信息插入成功  ' + str(datetime.datetime.now())

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


# 新增的模块
def branch(url, cursor, conn):
    if url not in old_branch:
        print u'爬取分支机构信息  ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'
                    continue
                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]
                    print corp_name
                    if html.text.__contains__('nav-main-branchCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-branchCount > span')[0].text

                        if int(num) % 10 == 0:
                            all_page_no = int(num) / 10
                        else:
                            all_page_no = int(num) / 10 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('branch', i, 10, corp_id)
                            # print soup2
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source
                                branch_name = re.findall('<span>(.*?)</span></a>', source)[0]
                                branch_url = re.findall('<a class="query_name" href="(.*?)" style=', source)[0]
                                legal_representative = re.findall('</a></td><td><span>(.*?)</span>', source)[0]
                                state = re.findall('<span class=".*?">(.*?)</span></td><td>', source)[0]
                                register_time = re.findall('<span class="">(.*?)</span></td></tr>', source)[0]
                                print branch_name

                                print branch_url
                                print legal_representative
                                print state
                                print register_time
                                cursor.execute(
                                    'insert into tyc_branch_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     branch_name,
                                     branch_url,
                                     legal_representative,
                                     state,
                                     register_time,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' 分支机构插入成功  ' + str(datetime.datetime.now())
                    else:
                        print corp_name + u' 没有分支机构信息  ' + str(datetime.datetime.now())
                        cursor.execute(
                            'insert into tyc_branch_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,

                             'no data',
                             'no data',
                             'no data',
                             'no data',
                             'no data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def core_team(url, cursor, conn):
    if url not in old_core_team:
        print u'爬取核心团队信息 ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                # print html.text
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'

                    continue

                else:

                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                    print corp_name
                    if html.text.__contains__('nav-main-companyTeammember'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-companyTeammember > span')[0].text

                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('teamMember', i, 5, corp_name)

                            res = soup2.select('div.team-item')
                            for x in range(len(res)):
                                source = str(res[x])
                                name = re.findall('img alt="(.*)" src', source)[0]
                                icon = re.findall('src="(.*?)"', source)[0]
                                position = re.findall('<div class="team-title">(.*?)</div><ul>', source)[0]
                                brief = \
                                    re.findall('<span class="text-dark-color">(.*?)</span></li></ul></div></div>',
                                               source)[
                                        0]
                                print name
                                print icon
                                print detag(position)
                                print detag(brief)

                                cursor.execute(
                                    'insert into tyc_core_team_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,
                                     name,
                                     icon,
                                     detag(position),
                                     detag(brief),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' - 核心团队信息插入成功  ' + str(datetime.datetime.now())

                    else:
                        print u'没有核心团队信息'
                        cursor.execute(
                            'insert into tyc_core_team_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def abnormal(url, cursor, conn):
    if url not in old_abnormal:
        print u'爬取经营异常信息 ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                # print html.text
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'

                    continue

                else:
                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                    print corp_name
                    if html.text.__contains__('nav-main-abnormalCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-abnormalCount > span')[0].text
                        print 'num: ' + num
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1

                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('abnormal', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source
                                table_len = str(len(soup2.select('tr > td')))
                                if table_len == '3':
                                    print u'只有3个'
                                    record_time = re_findall('<td><span>(.*?)</span></td>', source)[0]
                                    record_cause = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[0]
                                    record_department = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[1]
                                    cursor.execute(
                                        'insert into tyc_abnormal_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                        (url,
                                         corp_name,
                                         corp_id,

                                         record_time,
                                         record_cause,
                                         record_department,
                                         'no_data',
                                         'no_data',
                                         'no_data',

                                         str(datetime.datetime.now()),
                                         str(datetime.datetime.now())[:10])
                                    )
                                    conn.commit()
                                elif table_len == '6':
                                    print u'只有6个'
                                    record_time = re_findall('<td><span>(.*?)</span></td>', source)[0]
                                    record_cause = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[0]
                                    record_department = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[1]
                                    removal_time = re_findall('<td><span>(.*?)</span></td>', source)[1]
                                    removal_cause = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[2]
                                    removal_department = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[3]

                                    # print record_time
                                    # print record_cause
                                    # print record_department
                                    # print removal_time
                                    # print removal_cause
                                    # print removal_department

                                    cursor.execute(
                                        'insert into tyc_abnormal_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                        (url,
                                         corp_name,
                                         corp_id,

                                         record_time,
                                         record_cause,
                                         record_department,
                                         removal_time,
                                         removal_cause,
                                         removal_department,

                                         str(datetime.datetime.now()),
                                         str(datetime.datetime.now())[:10])
                                    )
                                    conn.commit()
                                else:
                                    pass
                            print corp_name + u' - 经营异常信息插入成功  ' + str(datetime.datetime.now())

                    else:
                        print u'没有经营异常信息'
                        cursor.execute(
                            'insert into tyc_abnormal_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,

                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()

                break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def illegal(url, cursor, conn):
    if url not in old_illegal:
        print u'爬取严重违法信息 ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                # print html.text
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'

                    continue

                else:

                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                    print corp_name

                    if html.text.__contains__('nav-main-illegalCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-illegalCount > span')[0].text
                        # print num
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('illegal', i, 5, corp_name)
                            # print soup2
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                record_time = re_findall('<td><span class="">(.*?)</span></td>', source)[0]
                                record_cause = \
                                    re_findall('<td><span class="">(.*?)</span></td>', source)[1]
                                record_department = \
                                    re_findall('<td><span class="">(.*?)</span></td>', source)[2]

                                print record_time
                                print record_cause
                                print record_department

                                cursor.execute(
                                    'insert into tyc_illegal_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     record_time,
                                     record_cause,
                                     record_department,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' - 严重违法信息插入成功  ' + str(datetime.datetime.now())

                    else:
                        print u'没有严重违法信息'
                        cursor.execute(
                            'insert into tyc_illegal_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,
                             'no_data',
                             'no_data',
                             'no_data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()

                    break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def equity(url, cursor, conn):
    if url not in old_equity:
        print u'爬取股权出质信息 ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'

                    continue

                else:

                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                    print corp_name

                    if html.text.__contains__('nav-main-equityCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-equityCount > span')[0].text
                        # print num
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('equity', i, 5, corp_name)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source
                                announcement_time = \
                                    re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[0]
                                register_no = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[1]
                                pledgor = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[2]
                                pledgee = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[3]
                                state = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[4]
                                print announcement_time
                                print register_no
                                print detag(pledgor)
                                print detag(pledgee)
                                print state

                                cursor.execute(
                                    'insert into tyc_equity_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     announcement_time,
                                     register_no,
                                     detag(pledgor),
                                     detag(pledgee),
                                     state,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' - 股权出质信息插入成功  ' + str(datetime.datetime.now())

                    else:
                        print u'没有股权出质信息'
                        cursor.execute(
                            'insert into tyc_equity_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,

                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()
                    break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def mortgage(url, cursor, conn):
    if url not in old_mortgage:
        print u'爬取动产抵押信息 ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                # print html.text
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'

                    continue

                else:

                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                    print corp_name

                    if html.text.__contains__('nav-main-mortgageCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-mortgageCount > span')[0].text

                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('mortgage', i, 5, corp_name)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                # print source
                                register_date = re_findall('<span class=" text-dark-color ">(.*?)</span></td>', source)[
                                    0]
                                register_num = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[0]
                                type = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[1]
                                amount = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[2]
                                register_department = \
                                    re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[3]

                                state = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[4]

                                # print register_date
                                # print register_num
                                # print type
                                # print amount
                                # print register_department
                                # print state
                                cursor.execute(
                                    'insert into tyc_mortgage_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     register_date,
                                     register_num,
                                     type,
                                     amount,
                                     register_department,
                                     state,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])

                                )
                                conn.commit()
                            print corp_name + u' - 动产抵押信息插入成功  ' + str(datetime.datetime.now())
                    else:
                        print u'没有动产抵押信息'
                        cursor.execute(
                            'insert into tyc_mortgage_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,

                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()
                    break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def towntax(url, cursor, conn):
    if url not in old_towntax:
        print u'爬取欠税公告信息 ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'

                    continue

                else:

                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                    print corp_name

                    if html.text.__contains__('nav-main-ownTaxCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-ownTaxCount > span')[0].text
                        # print num
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('towntax', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source
                                publish_date = \
                                    re_findall('<tr><td><span class=".*?text-dark-color.*?">(.*?)</span>', source)[0]
                                taxer_id = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[1]
                                own_tax_type = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[2]
                                tax_amount_now = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[
                                    3]
                                tax_balance = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[4]
                                tax_department = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[
                                    5]

                                print publish_date
                                print taxer_id
                                print own_tax_type
                                print tax_amount_now
                                print tax_balance
                                print tax_department

                                cursor.execute(
                                    'insert into tyc_towntax_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     publish_date,
                                     taxer_id,
                                     own_tax_type,
                                     tax_amount_now,
                                     tax_balance,
                                     tax_department,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' - 欠税公告信息插入成功  ' + str(datetime.datetime.now())

                    else:
                        print u'没有欠税公告信息'
                        cursor.execute(
                            'insert into tyc_towntax_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,

                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()
                    break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def taxCredit(url, cursor, conn):
    if url not in old_taxCredit:
        print u'爬取税务评级信息 ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'

                    continue

                else:

                    corp_name = re_findall(
                        '<span class="f18 in-block vertival-middle sec-c2" style="font-weight: 600">(.*?)</span>',
                        html.text)[0]
                    if corp_name == 'N':
                        print html.text

                    print corp_name

                    if html.text.__contains__('nav-main-taxCreditCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-taxCreditCount > span')[0].text
                        # print num
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('taxcredit', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source
                                year = re_findall('<tr><td><span>(.*?)</span></td>', source)[0]
                                tax_rating = re_findall('<td><span class="">(.*?)</span></td>', source)[0]
                                type = re_findall('<td><span class="">(.*?)</span></td>', source)[1]
                                taxer_id = re_findall('<td><span class="">(.*?)</span></td>', source)[2]
                                evaluation_unit = re_findall('<td><span class="">(.*?)</span></td>', source)[3]

                                print year
                                print tax_rating
                                print type
                                print taxer_id
                                print evaluation_unit

                                cursor.execute(
                                    'insert into tyc_taxcredit_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     year,
                                     tax_rating,
                                     type,
                                     taxer_id,
                                     evaluation_unit,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' - 税务评级信息插入成功  ' + str(datetime.datetime.now())

                    else:
                        print u'没有税务评级信息'
                        cursor.execute(
                            'insert into tyc_taxcredit_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,

                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()
                    break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def certificate(url, cursor, conn):
    if url not in old_certificate:
        print u'爬取资质证书信息 ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'

                    continue

                else:

                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                    print corp_name

                    if html.text.__contains__('nav-main-certificateCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-certificateCount > span')[0].text
                        # print num
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('certificate', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                # print source
                                certificate_type = \
                                    re_findall('<td><span class="c9 point hover_underline" onclick=".*?">(.*?)</span>',
                                               source)[0]
                                certificate_num = \
                                    re_findall('<span>(.*?)</span>', source)[0]
                                create_time = re_findall('<span>(.*?)</span>', source)[1]
                                invalid_time = re_findall('<span>(.*?)</span>', source)[2]

                                # print certificate_type
                                # print certificate_num
                                # print create_time
                                # print invalid_time

                                cursor.execute(
                                    'insert into tyc_certificate_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     certificate_type,
                                     certificate_num,
                                     create_time,
                                     invalid_time,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' - 资质证书信息插入成功  ' + str(datetime.datetime.now())

                    else:
                        print u'没有资质证书信息'
                        cursor.execute(
                            'insert into tyc_certificate_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,

                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()
                    break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def iconInfo(url, cursor, conn):
    if url not in old_iconInfo:
        print u'爬取商标信息 ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'

                    continue

                else:

                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                    print corp_name

                    if html.text.__contains__('nav-main-tmCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-tmCount > span')[0].text
                        # print num
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('tmInfo', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                register_date = re_findall('<td><span>(.*?)</span></td>', source)[0]
                                icon = re_findall(' src="(.*?)"/></td>', source)[0]
                                icon_name = re_findall('<td><span>(.*?)</span></td>', source)[1]
                                register_num = re_findall('<td><span>(.*?)</span></td>', source)[2]
                                type = re_findall('<td><span>(.*?)</span></td>', source)[3]
                                state = re_findall('<span class="in-block vertical-top">(.*?)</span></td>', source)[0]

                                cursor.execute(
                                    'insert into tyc_icon_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     register_date,
                                     icon,
                                     icon_name,
                                     register_num,
                                     type,
                                     state,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' - 商标信息插入成功  ' + str(datetime.datetime.now())

                    else:
                        print u'没有商标信息'
                        cursor.execute(
                            'insert into tyc_icon_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,

                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()
                    break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def checkInfo(url, cursor, conn):
    if url not in old_check:
        print u'爬取抽查检查信息 ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'

                    continue

                else:

                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                    print corp_name

                    if html.text.__contains__('nav-main-checkCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-checkCount > span')[0].text
                        # print num
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('check', i, 5, corp_name)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                date = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[0]
                                type = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[1]
                                result = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[2]
                                department = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[3]

                                print date
                                print type
                                print result
                                print department

                                cursor.execute(
                                    'insert into tyc_check_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     date,
                                     type,
                                     result,
                                     department,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' - 抽查检查信息插入成功  ' + str(datetime.datetime.now())

                    else:
                        print u'没有抽查检查信息'
                        cursor.execute(
                            'insert into tyc_check_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,

                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()
                    break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


def judicial(url, cursor, conn):
    if url not in old_judicial:
        print u'爬取司法拍卖信息 ' + str(datetime.datetime.now())
        corp_id = url.split('/company/')[1]

        while True:
            try:
                html = get_page(url)
                if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                    print 'parse error , retry'

                    continue

                else:

                    corp_name = re_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                    print corp_name

                    if html.text.__contains__('nav-main-judicialSaleCount'):
                        soup = BeautifulSoup(html.text, 'lxml')
                        num = soup.select('#nav-main-judicialSaleCount > span')[0].text
                        # print num
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('judicialSale', i, 5, corp_id)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source
                                auction_announcement_name = \
                                re_findall('<td><a class="in-block" href=".*?" target="_blank">(.*?)</a></td>', source)[
                                    0]

                                auction_announcement_url = \
                                re_findall('<td><a class="in-block" href="(.*?)" target="_blank">.*?</a></td>', source)[
                                    0]
                                date = re_findall('<td><span>(.*?)</span></td>', source)[0]
                                excute_court = \
                                re_findall('<td><span class="text-dark-color in-block">(.*?)</span></td>', source)[0]
                                auction_target = \
                                re_findall('<td><div class="text-dark-color in-block">(.*?)</div></td>', source)[0]

                                print auction_announcement_name
                                print auction_announcement_url
                                print date
                                print excute_court
                                print detag(auction_target)
                                cursor.execute(
                                    'insert into tyc_judicial_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (url,
                                     corp_name,
                                     corp_id,

                                     auction_announcement_name,
                                     auction_announcement_url,
                                     date,
                                     excute_court,
                                     detag(auction_target),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' - 司法拍卖信息插入成功  ' + str(datetime.datetime.now())

                    else:
                        print u'没有司法拍卖信息'
                        cursor.execute(
                            'insert into tyc_judicial_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                            (url,
                             corp_name,
                             corp_id,

                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',
                             'no_data',

                             str(datetime.datetime.now()),
                             str(datetime.datetime.now())[:10])
                        )
                        conn.commit()
                    break
            except Exception, e:
                if str(e).find('HTTPSConnectionPool') >= 0:
                    print 'HTTPSConnectionPool , retry '
                    continue
                elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                    return 'MySql error 2006 ,restart'
                elif str(e).find('2003') >= 0:
                    return 'MySql error 2003 ,restart'
                elif str(e).find('Connection aborted') >= 0:
                    print 'Connection aborted occured~~~!!!'
                    continue
                else:
                    print url + ' unknown error with info ' + str(e)
                    print traceback.format_exc()
                    break


###################################################################################
def get_cookie_by_name(name, page_no, per_page, company_name):
    while True:
        try:
            proxies1 = get_proxy()
            timestamp = int(time.time() * 1000)
            head1 = {
                'Content-Type': 'application/json; charset=UTF-8',
                'Host': 'www.tianyancha.com',
                'Origin': 'https://www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'X-Requested-With': 'XMLHttpRequest'
            }
            tongji_url = "https://www.tianyancha.com/tongji/" + urllib.quote(
                company_name.encode('utf8')) + ".json?_=" + str(timestamp)

            tongji_page = requests.get(tongji_url, headers=head1, proxies=proxies1, verify=False)

            cookie = tongji_page.cookies.get_dict()
            js_code = "".join([chr(int(code)) for code in tongji_page.json()["data"].split(",")])

            token = re.findall(r"token=(\w+);", js_code)[0]
            utm_code = re.findall("return'([^']*?)'", js_code)[0]
            t = ord(company_name[0])
            fw = open("D:\PycharmProjects\\new_tianyancha\\rsid\\" + name + "_rsid.js", "wb+")
            # fw = open("/Users/huaiz/PycharmProjects/new_tianyancha/rsid/" + name + "_rsid.js", "wb+")
            fw.write('var t = "' + str(t) + '",wtf = "' + utm_code + '";' + static_js_code)
            fw.close()

            phantomResStr = execCmd('phantomjs D:\PycharmProjects\\new_tianyancha\\rsid\\' + name + '_rsid.js')
            # phantomResStr = execCmd('phantomjs /Users/huaiz/PycharmProjects/new_tianyancha/rsid/' + name + '_rsid.js')
            # --print phantomResStr
            # print "phantomResStr: %s" % phantomResStr

            phantomRes = json.loads(phantomResStr)
            ssuid = phantomRes["ssuid"]
            utm = phantomRes["utm"]

            head2 = {
                'Host': 'www.tianyancha.com',
                # 'Referer': 'https://www.tianyancha.com/company/22822',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
                'Accept-Language': 'zh-CN,zh;q=0.8',
                'Connection': 'keep-alive',
                'Accept': '*/*',
                'Accept-Encoding': 'gzip, deflate, sdch, br',
                'Cookie': 'ssuid=' + ssuid + '; token=' + token + '; _utm=' + utm + '; aliyungf_tc=' + cookie[
                    "aliyungf_tc"] + '; TYCID=' + cookie["TYCID"] + '; csrfToken=' + cookie[
                              "csrfToken"] + '; uccid=baeee58fe4d1d697092e61f6525e8719',
                'X-Requested-With': 'XMLHttpRequest'
            }

            url = 'https://www.tianyancha.com/pagination/' + name + '.xhtml?ps=' + str(per_page) + '&pn=' + str(
                page_no) + '&name=' + urllib.quote(company_name.encode('utf8')) + '&_=' + str(timestamp - 1)
            print url + '_@_' + str(datetime.datetime.now())
            resp = requests.get(url, headers=head2, proxies=proxies1, verify=False)
            # print resp
            html = resp.text
            soup2 = BeautifulSoup(html, 'lxml')
            return soup2
            break
        except Exception, e:
            if str(e).find('list index out of range') >= 0:
                print u'get cookie 失败 重试'
            continue


def get_cookie_by_id(name, page_no, per_page, corp_id):
    while True:
        try:
            proxies1 = get_proxy()
            timestamp = int(time.time() * 1000)
            head1 = {
                'Content-Type': 'application/json; charset=UTF-8',
                'Host': 'www.tianyancha.com',
                'Origin': 'https://www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'X-Requested-With': 'XMLHttpRequest'
            }
            tongji_url = "https://www.tianyancha.com/tongji/" + corp_id + ".json?_=" + str(timestamp)

            tongji_page = requests.get(tongji_url, headers=head1, proxies=proxies1, verify=False)

            cookie = tongji_page.cookies.get_dict()
            js_code = "".join([chr(int(code)) for code in tongji_page.json()["data"].split(",")])

            token = re.findall(r"token=(\w+);", js_code)[0]
            utm_code = re.findall("return'([^']*?)'", js_code)[0]
            t = ord(corp_id[0])

            fw = open("D:\PycharmProjects\\new_tianyancha\\rsid\\" + name + "_rsid.js", "wb+")
            # fw = open("/Users/huaiz/PycharmProjects/new_tianyancha/rsid/" + name + "_rsid.js", "wb+")
            fw.write('var t = "' + str(t) + '",wtf = "' + utm_code + '";' + static_js_code)
            fw.close()
            phantomResStr = execCmd('phantomjs D:\PycharmProjects\\new_tianyancha\\rsid\\' + name + '_rsid.js')
            # phantomResStr = execCmd('phantomjs /Users/huaiz/PycharmProjects/new_tianyancha/rsid/' + name + '_rsid.js')
            # --print phantomResStr
            # print "phantomResStr: %s" % phantomResStr
            phantomRes = json.loads(phantomResStr)
            ssuid = phantomRes["ssuid"]
            utm = phantomRes["utm"]

            head2 = {
                'Host': 'www.tianyancha.com',
                # 'Referer': 'https://www.tianyancha.com/company/22822',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
                'Accept-Language': 'zh-CN,zh;q=0.8',
                'Connection': 'keep-alive',
                'Accept': '*/*',
                'Accept-Encoding': 'gzip, deflate, sdch, br',
                'Cookie': 'ssuid=' + ssuid + '; token=' + token + '; _utm=' + utm + '; aliyungf_tc=' + cookie[
                    "aliyungf_tc"] + '; TYCID=' + cookie["TYCID"] + '; csrfToken=' + cookie[
                              "csrfToken"] + '; uccid=baeee58fe4d1d697092e61f6525e8719',
                'X-Requested-With': 'XMLHttpRequest'
            }

            url = 'https://www.tianyancha.com/pagination/' + name + '.xhtml?ps=' + str(per_page) + '&pn=' + str(
                page_no) + '&id=' + corp_id + '&_=' + str(timestamp - 1)
            print url + '_@_' + str(datetime.datetime.now())
            resp = requests.get(url, headers=head2, proxies=proxies1, verify=False)
            # print resp
            html = resp.text
            soup2 = BeautifulSoup(html, 'lxml')
            return soup2
            break
        except Exception, e:
            if str(e).find('list index out of range') >= 0:
                print u'get cookie 失败 重试'
            continue


def crawl(url):
    while True:
        conn = MySQLdb.connect(host=db_config.server09_host, port=db_config.server09_port, user=db_config.server09_user,
                               passwd=db_config.server09_passwd,
                               db=db_config.server09_dbname,
                               charset="utf8")
        cursor = conn.cursor()
        print 'parsing with : ' + url

        # -----------------------------------------------------------------------------

        # jingpin(url, cursor, conn)
        # if jingpin == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif jingpin == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        #
        # staff(url, cursor, conn)
        # if staff == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif staff == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # shareholder(url, cursor, conn)
        # if shareholder == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif shareholder == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # out_invest(url, cursor, conn)
        # if out_invest == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif out_invest == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # change_info(url, cursor, conn)
        # if change_info == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif change_info == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # product_info(url, cursor, conn)
        # if product_info == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif product_info == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #####-----------------------------------first insert
        # wechat_info(url, cursor, conn)
        # if wechat_info == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif wechat_info == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # website_record(url, cursor, conn)
        # if website_record == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif website_record == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # lawsuit(url, cursor, conn)
        # if lawsuit == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif lawsuit == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # court(url, cursor, conn)
        # if court == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif court == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # zhixing(url, cursor, conn)
        # if zhixing == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif zhixing == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # -----------------------------------second insert
        # announcement(url, cursor, conn)
        # if announcement == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif announcement == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # punish(url, cursor, conn)
        # if punish == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif punish == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # recruit(url, cursor, conn)
        # if recruit == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif recruit == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # patent(url, cursor, conn)
        # if patent == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif patent == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # copyR(url, cursor, conn)
        # if copyR == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif copyR == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # copyrightWorks(url, cursor, conn)
        # if copyrightWorks == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif copyrightWorks == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # firmProduct(url, cursor, conn)
        # if firmProduct == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif firmProduct == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # investment_info(url, cursor, conn)
        # if investment_info == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif investment_info == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # dishonest(url, cursor, conn)
        # if dishonest == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif dishonest == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # rongziHistory(url, cursor, conn)
        # if rongziHistory == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif rongziHistory == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue

        # -----------------------------------新增的模块

        branch(url, cursor, conn)
        if branch == 'MySql error 2003 ,restart':
            print '2003/2006,retry'
            continue
        elif branch == 'MySql error 2006 ,restart':
            print '2003/2006,retry'
            continue

        core_team(url, cursor, conn)
        if core_team == 'MySql error 2003 ,restart':
            print '2003/2006,retry'
            continue
        elif core_team == 'MySql error 2006 ,restart':
            print '2003/2006,retry'
            continue
        #
        abnormal(url, cursor, conn)
        if abnormal == 'MySql error 2003 ,restart':
            print '2003/2006,retry'
            continue
        elif abnormal == 'MySql error 2006 ,restart':
            print '2003/2006,retry'
            continue

        illegal(url, cursor, conn)
        if illegal == 'MySql error 2003 ,restart':
            print '2003/2006,retry'
            continue
        elif illegal == 'MySql error 2006 ,restart':
            print '2003/2006,retry'
            continue
        # -------------------first ---------------
        #
        # equity(url, cursor, conn)
        # if equity == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif equity == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # mortgage(url, cursor, conn)
        # if mortgage == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif mortgage == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # towntax(url, cursor, conn)
        # if towntax == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif towntax == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # taxCredit(url, cursor, conn)--
        # if taxCredit == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif taxCredit == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # -----------------------second----------------------

        # certificate(url, cursor, conn)
        # if certificate == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif certificate == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # iconInfo(url, cursor, conn)
        # if iconInfo == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif iconInfo == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # checkInfo(url, cursor, conn)
        # if checkInfo == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif checkInfo == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        #
        # judicial(url, cursor, conn)
        # if judicial == 'MySql error 2003 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # elif judicial == 'MySql error 2006 ,restart':
        #     print '2003/2006,retry'
        #     continue
        # -------------------------------------------------------------------------------
        # cursor.execute(
        #     'insert into tyc_log_info values ("%s","%s","%s")' %
        #     (url,
        #      str(datetime.datetime.now()),
        #      str(datetime.datetime.now())[:10])
        # )
        # print '录入log表'
        # conn.commit()

        print url + u' 各模块处理结束~~~~~~~  ' + str(datetime.datetime.now())
        cursor.close()
        conn.close()
        break


if __name__ == "__main__":
    conn = MySQLdb.connect(host=db_config.server09_host, port=db_config.server09_port, user=db_config.server09_user,
                           passwd=db_config.server09_passwd,
                           db=db_config.server09_dbname,
                           charset="utf8")
    cursor = conn.cursor()

    cursor.execute('select corp_url from tyc_prepare_info')
    all_urls = []
    urls = cursor.fetchall()
    for y in range(0, len(urls)):
        all_urls.append(urls[y][0])

    print u'全体待爬url准备完毕 ！ @_ ' + str(datetime.datetime.now())
    time.sleep(5)
    # ==============OLD_URLS==============================================

    # cursor.execute('select corp_url from tyc_jingpin_info')
    # old_jingpin = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_jingpin.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_staff_info')
    # old_staff = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_staff.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_shareholder_info')
    # old_shareholder = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_shareholder.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_out_investment_info')
    # old_out_investment = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_out_investment.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_change_record_info')
    # old_change_record = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_change_record.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_product_info')
    # old_product = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_product.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_wechat_info')
    # old_wechat = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_wechat.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_webrecord_info')
    # old_webrecord = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_webrecord.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_lawsuit_info')
    # old_lawsuit = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_lawsuit.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_court_info')
    # old_court = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_court.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_zhixing_info')
    # old_zhixing = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_zhixing.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_announcement_info')
    # old_announcement = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_announcement.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_punish_info')
    # old_punish = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_punish.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_recruit_info')
    # old_recruit = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_recruit.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_patent_info')
    # old_patent = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_patent.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_copyRcount_info')
    # old_copyRcount = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_copyRcount.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_copyRworks_info')
    # old_copyRworks = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_copyRworks.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_companyProduct_info')
    # old_companyProduct = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_companyProduct.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_investment_info')
    # old_investment = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_investment.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_dishonest_info')
    # old_dishonest = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_dishonest.append(old[y][0])
    #
    # cursor.execute('select corp_url from tyc_rongziCompany_info')
    # old_rongziCompany = []
    # old = cursor.fetchall()
    # for y in range(0, len(old)):
    #     old_rongziCompany.append(old[y][0])

    # 新增的模块
    cursor.execute('select corp_url from tyc_branch_info')
    old_branch = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_branch.append(old[y][0])

    cursor.execute('select corp_url from tyc_core_team_info')
    old_core_team = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_core_team.append(old[y][0])

    cursor.execute('select corp_url from tyc_abnormal_info')
    old_abnormal = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_abnormal.append(old[y][0])

    cursor.execute('select corp_url from tyc_illegal_info')
    old_illegal = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_illegal.append(old[y][0])

    cursor.execute('select corp_url from tyc_equity_info')
    old_equity = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_equity.append(old[y][0])

    cursor.execute('select corp_url from tyc_mortgage_info')
    old_mortgage = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_mortgage.append(old[y][0])

    cursor.execute('select corp_url from tyc_townTax_info')
    old_towntax = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_towntax.append(old[y][0])

    cursor.execute('select corp_url from tyc_taxCredit_info')
    old_taxCredit = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_taxCredit.append(old[y][0])

    cursor.execute('select corp_url from tyc_certificate_info')
    old_certificate = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_certificate.append(old[y][0])

    cursor.execute('select corp_url from tyc_icon_info')
    old_iconInfo = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_iconInfo.append(old[y][0])

    cursor.execute('select corp_url from tyc_check_info')
    old_check = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_check.append(old[y][0])

    cursor.execute('select corp_url from tyc_judicial_info')
    old_judicial = []
    old = cursor.fetchall()
    for y in range(0, len(old)):
        old_judicial.append(old[y][0])

    cursor.close()
    conn.close()

    print u'已爬url准备完毕 ！ @_ ' + str(datetime.datetime.now())

    # conn = MySQLdb.connect(host=config.server09_host, port=config.server09_port, user=config.server09_user,
    #                        passwd=config.server09_passwd,
    #                        db=config.server09_dbname,
    #                        charset="utf8")
    # cursor = conn.cursor()

    #
    thread_num = 5
    start_no = 0
    end_no = len(all_urls)

    while start_no < (end_no - thread_num + 1):
        threads = []

        for inner_index in range(0, thread_num):
            threads.append(threading.Thread(target=crawl, args=(all_urls[start_no + inner_index],)))
        for t in threads:
            t.setDaemon(True)
            t.start()
        t.join()
        start_no += thread_num

    # conn = MySQLdb.connect(host="139.198.189.129", port=20009, user="root", passwd="somao1129",
    #                        db="tianyancha", charset="utf8")

    # # url = 'https://www.tianyancha.com/company/31757870'  # 摩拜
    # url = 'https://www.tianyancha.com/company/2310296799'
    # url = 'https://www.tianyancha.com/company/10686233'   # 万达
    # crawl(url)
