# -*- coding: utf-8 -*-
# @Time         : 2018/3/29 09:57
# @Author       : Huaiz
# @Email        : Apokar@163.com
# @File         : tyc_3.0_prepare_info.py
# @Software     : PyCharm Community Edition
# @PROJECT_NAME : new_tianyancha


import re
import sys
import db_config
import random
import urllib
import MySQLdb
import datetime
import requests
import traceback
from multiprocessing import Pool
reload(sys)
sys.setdefaultencoding('utf8')

proxy_list = list(set(urllib.urlopen(
    'http://60.205.92.109/api.do?name=3E30E00CFEDCD468E6862270F5E728AF&status=1&type=static').read().split('\n')[:-1]))


def detag(html):
    detag = re.subn('<[^>]*>', '', html)[0]
    detag = detag.replace('&nbsp;', ' ')
    detag = detag.replace("'", '|')
    detag = detag.replace('&ensp;', ';')
    detag = detag.replace('\t', '')
    detag = detag.replace('\n', '')
    detag = detag.replace('\r', '')
    return detag


# 把信息放入tyc_prepare_info中
def search(keyword, conn, cursor, proxy_list):
    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }
            url = 'https://www.tianyancha.com/search?key=' + urllib.quote(
                keyword.encode('utf8')) + '&checkFrom=searchBox'
            print url
            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(url, proxies=proxies, timeout=10, headers=headers)
            if html.text.find('search-multi-filter ') == -1:
                print "not search page"
                continue
            break
        except Exception as e:
            print 'error :\t%s' % e
            print traceback.format_exc()
            continue
    results = re.findall('<div class="search_right_item[^"]*"><div><a href="(.*?)"[^>]*>([\w\W]*?)</a>', html.text,
                         re.S)
    print "%s results found!" % len(results)

    for result in results:
        corp_name = detag(result[1])
        corp_id = result[0].split('/company/')[1]
        cursor.execute("insert into tyc_prepare_info values ('%s','%s','%s','%s','%s','%s')" % (
            keyword, result[0], corp_id, corp_name, str(datetime.datetime.now()), str(datetime.datetime.now())[:10]))
        print u'%s 的名称 天眼查id 已插入到prepare表中' % corp_name
    conn.commit()


def searchWorker(pairs, proxy_list):
    conn = MySQLdb.connect(host=db_config.server09_host, port=db_config.server09_port, user=db_config.server09_user,
                           passwd=db_config.server09_passwd,
                           db=db_config.server09_dbname,
                           charset="utf8")
    cursor = conn.cursor()
    for pair in pairs:
        search(pair[0], conn, cursor, proxy_list)
    cursor.close()
    conn.close()


if __name__ == '__main__':
    p = Pool()
    pNum = 40
    span = 50000

    conn = MySQLdb.connect(host=db_config.server09_host, port=db_config.server09_port, user=db_config.server09_user,
                           passwd=db_config.server09_passwd,
                           db=db_config.server09_dbname,
                           charset="utf8")
    cursor = conn.cursor()

    cursor.execute(
        "select distinct a.corp_name from keyword_list_table a left join tyc_prepare_info b on a.corp_name=b.corp_name where b.corp_name is null limit " + str(
            pNum * span))
    # cursor.execute(
    #     'insert into tyc_searched_log values ("%s","%s","%s","%s","%s")' %
    #     (
    #         '0',         # 设为0, 该字段自增
    #         corp_name,
    #         corp_id,
    #
    #         str(datetime.datetime.now()),
    #         str(datetime.datetime.now())[:10])
    # )
    # conn.commit()
    # conn.close()
    pairs = cursor.fetchall()
    print '%s records found!' % (len(pairs))

    cursor.close()
    conn.close()

    for i in range(pNum):
        p.apply_async(searchWorker, (pairs[i * span:(i + 1) * span], proxy_list,))  # 增加新的进程
        # p.apply_async(getBaseInfoWorker, (pairs[i*span:(i+1)*span],proxy_list,))  #增加新的进程
    p.close()  # 禁止在增加新的进程
    p.join()
    print "pool process done"
    quit()


# if __name__ == '__main__':
#     pairs = [['https://www.tianyancha.com/company/2985973087']]
#     searchWorker(pairs, proxy_list)
