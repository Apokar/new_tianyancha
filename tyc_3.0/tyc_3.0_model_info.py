# -*- coding: utf-8 -*-
# @Time         : 2018/3/29 16:18
# @Author       : HuaiZ
# @Email        : Apokar@163.com
# @File         : tyc_3.0_model_info.py
# @Software     : PyCharm Community Edition
# @PROJECT_NAME : new_tianyancha
# @brief        : 模块化信息采集
import os
import re
import time
import json
import db_config
import random
import urllib
import MySQLdb
import datetime
import requests
import traceback
from bs4 import BeautifulSoup
from multiprocessing import Pool

import sys

reload(sys)
sys.setdefaultencoding('utf8')

import urllib3

urllib3.disable_warnings()

proxy_list = list(set(urllib.urlopen(
    'http://60.205.92.109/api.do?name=3E30E00CFEDCD468E6862270F5E728AF&status=1&type=static').read().split('\n')[:-1]))

static_js_code = """
var ne = "2633141825201321121345332721524273528936811101916293117022304236|1831735156281312241132340102520529171363214283321272634162219930|2332353860219720155312141629130102234183691124281413251227261733|2592811262018293062732141927100364232411333831161535317211222534|9715232833130331019112512913172124126035262343627321642220185148|3316362031032192529235212215274341412306269813312817111724201835|3293412148301016132183119242311021281920736172527353261533526224|3236623313013201625221912357142415851018341117262721294332103928|2619332514511302724163415617234183291312001227928218353622321031|3111952725113022716818421512203433241091723133635282932601432216";
var base64chars = "abcdefghijklmnopqrstuvwxyz1234567890-~!";
var _0x4fec = "f9D1x1Z2o1U2f5A1a1P1i7R1u2S1m1F1,o2A1x2F1u5~j1Y2z3!p2~r3G2m8S1c1,i3E5o1~d2!y2H1e2F1b6`g4v7,p1`t7D3x5#w2~l2Z1v4Y1k4M1n1,C2e3P1r7!s6U2n2~p5X1e3#,g4`b6W1x4R1r4#!u5!#D1f2,!z4U1f4`f2R2o3!l4I1v6F1h2F1x2!,b2~u9h2K1l3X2y9#B4t1,t5H1s7D1o2#p2#z1Q3v2`j6,r1#u5#f1Z2w7!r7#j3S1";
rs_decode = function(e) {
    return ne.split("|")[e]
};
    var r = t+"";
    r = r.length > 1 ? r[1] : r;
    for (var i = rs_decode(r), o = _0x4fec.split(",")[r], a = [], s = 0, u = 0; u < o.length; u++) {
        if ("`" != o[u] && "!" != o[u] && "~" != o[u] || (a.push(i.substring(s, s + 1)), s++), "#" == o[u] && (a.push(i.substring(s, s + 1)), a.push(i.substring(s + 1, s + 3)), a.push(i.substring(s + 3, s + 4)), s += 4), o.charCodeAt(u) > 96 && o.charCodeAt(u) < 123) for (var l = o[u + 1], c = 0; c < l; c++) a.push(i.substring(s, s + 2)),
        s += 2;
        if (o.charCodeAt(u) > 64 && o.charCodeAt(u) < 91) for (var l = o[u + 1], c = 0; c < l; c++) a.push(i.substring(s, s + 1)),
        s++
    }
    rsid = a;
for (var chars = "",  i = 0; i < rsid.length; i++) chars += base64chars[rsid[i]];
for (var fxck = wtf.split(","), fxckStr = "", i = 0; i < fxck.length; i++) fxckStr += chars[fxck[i]];
var utm = fxckStr;
console.log("{\\"utm\\":\\""+utm+"\\",\\"ssuid\\":\\""+Math.round(2147483647 * Math.random()) * (new Date).getUTCMilliseconds() % 1e10+"\\"}")
phantom.exit();
"""


def execCmd(cmd):
    text = os.popen(cmd).read()
    return (text)


def reS_findall(pattern, html):
    if re.findall(pattern, html, re.S):
        return re.findall(pattern, html, re.S)
    else:
        return 'N'


def re_findall(pattern, html):
    if re.findall(pattern, html):
        return re.findall(pattern, html)
    else:
        return 'N'


def get_num(str):
    b = re.compile(u"[0123456789.]")
    c = "".join(b.findall(str.decode('utf8')))
    return c


def get_chinese(str):
    b = re.compile(u"[\u4e00-\u9fa5]*")
    c = "".join(b.findall(str.decode('utf8')))
    return c


def detag(html):
    detag = re.subn('<[^>]*>', '', html)[0]
    detag = detag.replace('&nbsp;', ' ')
    detag = detag.replace("'", '|')
    detag = detag.replace('&ensp;', ';')
    detag = detag.replace('\t', '')
    detag = detag.replace('\n', '')
    detag = detag.replace('\r', '')
    detag = detag.replace('<!--', '')
    return detag


def get_code(fake, proxies):
    url = 'https://www.tianyancha.com/company/26519579'
    while True:
        try:

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            content_code = requests.get(url, proxies=proxies, timeout=10, headers=headers)

            if content_code != None:
                if content_code.text.__contains__('https://static.tianyancha.com/wap/images/notFound.png'):
                    print u'nofound.img'
                    break
                elif content_code.text.__contains__('https://m.tianyancha.com/login'):
                    print u'需要登录 重试一次'
                    time.sleep(2)
                    continue
                else:
                    # print str(content_1.text)
                    register_time = re_findall('<text class="tyc-num" >(.*?)</text>', str(content_code.text))[1]
                    # print register_time
                    approval_date = re_findall('<text class="tyc-num" >(.*?)</text></td>', str(content_code.text))[0]
                    # print approval_date
                    decode_dict = {
                        register_time[0]: '1',
                        approval_date[0]: '2',
                        register_time[6]: '3',
                        register_time[9]: '4',
                        register_time[3]: '5',
                        approval_date[9]: '6',
                        approval_date[3]: '7',
                        register_time[2]: '8',
                        register_time[1]: '9',
                        approval_date[1]: '0',
                        '.': '.',
                        '-': '-'
                    }

                    real_content = ''
                    danwei = get_chinese(fake)
                    fake_shuzi = str(get_num(fake))

                    for j in fake_shuzi:
                        real_content += decode_dict.get(j)
                    print u'解密成功'
                    return real_content + danwei
            break
        except Exception, e:

            if str(e).__contains__('HTTPSConnectionPool'):
                print 'HTTPSConnectionPool error ,retry'
                continue
            print traceback.format_exc()
            print u'解密失败,返回原数字'
            return fake
            break


def get_auth_token():
    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies1 = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }
            headers = {
                'Host': 'www.tianyancha.com',
                'Connection': 'keep-alive',
                'Content-Length': '105',
                'Accept': '*/*',
                'Origin': 'https://www.tianyancha.com',
                'X-Requested-With': 'XMLHttpRequest',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
                'Content-Type': 'application/json; charset=UTF-8',
                'Referer': 'https://www.tianyancha.com',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-CN,zh;q=0.9',

            }

            data1 = {
                'autoLogin': 'true',
                'cdpassword': "5d93ceb70e2bf5daa84ec3d0cd2c731a",
                'loginway': "PL",
                'mobile': "18551986625"
            }
            data2 = {
                'autoLogin': 'true',
                'cdpassword': "5d93ceb70e2bf5daa84ec3d0cd2c731a",
                'loginway': "PL",
                'mobile': "15195885557"
            }
            data3 = {
                'autoLogin': 'true',
                'cdpassword': "5d93ceb70e2bf5daa84ec3d0cd2c731a",
                'loginway': "PL",
                'mobile': "17183463760"
            }
            data4 = {
                'autoLogin': 'true',
                'cdpassword': "5d93ceb70e2bf5daa84ec3d0cd2c731a",
                'loginway': "PL",
                'mobile': "15578146934"
            }
            data5 = {
                'autoLogin': 'true',
                'cdpassword': "5d93ceb70e2bf5daa84ec3d0cd2c731a",
                'loginway': "PL",
                'mobile': "15005187360"
            }

            data = random.choice([data1,data2,data3,data4,data5])
            print u'用 ' + str(data['mobile']) + u' 登录'

            data = json.dumps(data)

            login_url = 'https://www.tianyancha.com/cd/login.json'

            resp = requests.post(login_url, headers=headers, data=data, proxies=proxies1)
            token = resp.content
            token = json.loads(token)
            auth_token = token['data']['token']
            print u'返回了新的auth_token _@_ ' + str(datetime.datetime.now())
            return auth_token
        except Exception, e:

            print 'error with get_auth_token '
            print str(e)
            continue


def get_cookie_by_id(name, page_no, per_page, corp_id, auth_token):
    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies1 = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            timestamp = int(time.time() * 1000)
            head1 = {
                'Content-Type': 'application/json; charset=UTF-8',
                'Host': 'www.tianyancha.com',
                'Origin': 'https://www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'X-Requested-With': 'XMLHttpRequest'
            }
            tongji_url = "https://www.tianyancha.com/tongji/" + corp_id + ".json?_=" + str(timestamp)

            tongji_page = requests.get(tongji_url, headers=head1, proxies=proxies1, verify=False, timeout=8)

            cookie = tongji_page.cookies.get_dict()

            js_code = "".join([chr(int(code)) for code in tongji_page.json()["data"].split(",")])

            token = re.findall(r"token=(\w+);", js_code)[0]
            utm_code = re.findall("return'([^']*?)'", js_code)[0]
            t = ord(corp_id[0])
            fw = open("D:\PycharmProjects\\new_tianyancha\\rsid\\" + name + "_rsid.js", "wb+")
            # fw = open("/Users/huaiz/PycharmProjects/new_tianyancha/rsid/" + name + "_rsid.js", "wb+")

            fw.write('var t = "' + str(t) + '",wtf = "' + utm_code + '";' + static_js_code)
            fw.close()
            phantomResStr = execCmd('phantomjs D:\PycharmProjects\\new_tianyancha\\rsid\\' + name + '_rsid.js')
            # phantomResStr = execCmd('phantomjs /Users/huaiz/PycharmProjects/new_tianyancha/rsid/' + name + '_rsid.js')
            # --print phantomResStr
            # print "phantomResStr: %s" % phantomResStr
            phantomRes = json.loads(phantomResStr)
            ssuid = phantomRes["ssuid"]
            utm = phantomRes["utm"]
            head2 = {
                'Host': 'www.tianyancha.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
                'Accept-Language': 'zh-CN,zh;q=0.8',
                'Connection': 'keep-alive',
                'Accept': '*/*',
                'Accept-Encoding': 'gzip, deflate, sdch, br',
                'Cookie':
                    'ssuid=' + ssuid
                    + '; token=' + token
                    + '; _utm=' + utm
                    + '; aliyungf_tc=' + cookie["aliyungf_tc"]
                    + '; TYCID=' + cookie["TYCID"]
                    + '; csrfToken=' + cookie["csrfToken"]
                    + '; uccid=70778d0922113b31f4a0331d3a71b274'
                    + '; auth_token=' + auth_token,

                'X-Requested-With': 'XMLHttpRequest'
            }

            url = 'https://www.tianyancha.com/pagination/' + name + '.xhtml?ps=' + str(per_page) + '&pn=' + str(
                page_no) + '&id=' + corp_id + '&_=' + str(timestamp - 1)
            print url + ' _@_ ' + str(datetime.datetime.now())
            resp = requests.get(url, headers=head2, proxies=proxies1, verify=False, timeout=8)
            if resp.status_code == 200:
                html = resp.text
                soup2 = BeautifulSoup(html, 'lxml')
                return soup2
                break
            elif resp.status_code == 401:
                print u'auth_token 失效,重新获取'
                auth_token = get_auth_token()
                continue
            elif resp.status_code == 403:
                print '该账户需要验证码验证'
                auth_token = get_auth_token()
                continue
            else:
                print '之前没发现的错误 '
                print 'VVVVVVVVVVVVVVVVV'
                print resp.status_code
                print resp.text
                print '^^^^^^^^^^^^^^^^'
                auth_token = get_auth_token()
                break


        except Exception, e:

            if str(e).find('list index out of range') >= 0:
                print u'get cookie 失败 重试'
                continue
            elif str(e).find('HTTPSConnectionPool') >= 0:
                print u'HTTPSConnectionPool'
                continue
            else:
                print traceback.format_exc()


def get_cookie_by_name(name, page_no, per_page, company_name, auth_token):
    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies1 = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }
            timestamp = int(time.time() * 1000)
            head1 = {
                'Content-Type': 'application/json; charset=UTF-8',
                'Host': 'www.tianyancha.com',
                'Origin': 'https://www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'X-Requested-With': 'XMLHttpRequest'
            }
            tongji_url = "https://www.tianyancha.com/tongji/" + urllib.quote(
                company_name.encode('utf8')) + ".json?_=" + str(timestamp)

            tongji_page = requests.get(tongji_url, headers=head1, proxies=proxies1, verify=False, timeout=8)

            cookie = tongji_page.cookies.get_dict()
            js_code = "".join([chr(int(code)) for code in tongji_page.json()["data"].split(",")])

            token = re.findall(r"token=(\w+);", js_code)[0]
            utm_code = re.findall("return'([^']*?)'", js_code)[0]
            t = ord(company_name[0])
            fw = open("D:\PycharmProjects\\new_tianyancha\\rsid\\" + name + "_rsid.js", "wb+")
            # fw = open("/Users/huaiz/PycharmProjects/new_tianyancha/rsid/" + name + "_rsid.js", "wb+")
            fw.write('var t = "' + str(t) + '",wtf = "' + utm_code + '";' + static_js_code)
            fw.close()

            phantomResStr = execCmd('phantomjs D:\PycharmProjects\\new_tianyancha\\rsid\\' + name + '_rsid.js')
            # phantomResStr = execCmd('phantomjs /Users/huaiz/PycharmProjects/new_tianyancha/rsid/' + name + '_rsid.js')
            # --print phantomResStr
            # print "phantomResStr: %s" % phantomResStr

            phantomRes = json.loads(phantomResStr)
            ssuid = phantomRes["ssuid"]
            utm = phantomRes["utm"]

            head2 = {
                'Host': 'www.tianyancha.com',
                # 'Referer': 'https://www.tianyancha.com/company/22822',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
                'Accept-Language': 'zh-CN,zh;q=0.8',
                'Connection': 'keep-alive',
                'Accept': '*/*',
                'Accept-Encoding': 'gzip, deflate, sdch, br',
                'Host': 'www.tianyancha.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
                'Accept-Language': 'zh-CN,zh;q=0.8',
                'Connection': 'keep-alive',
                'Accept': '*/*',
                'Accept-Encoding': 'gzip, deflate, sdch, br',
                'Cookie':
                    'ssuid=' + ssuid
                    + '; token=' + token
                    + '; _utm=' + utm
                    + '; aliyungf_tc=' + cookie["aliyungf_tc"]
                    + '; TYCID=' + cookie["TYCID"]
                    + '; csrfToken=' + cookie["csrfToken"]
                    + '; uccid=70778d0922113b31f4a0331d3a71b274'
                    + '; auth_token=' + auth_token,
                'X-Requested-With': 'XMLHttpRequest'
            }

            url = 'https://www.tianyancha.com/pagination/' + name + '.xhtml?ps=' + str(per_page) + '&pn=' + str(
                page_no) + '&name=' + urllib.quote(company_name.encode('utf8')) + '&_=' + str(timestamp - 1)
            print url + '_@_' + str(datetime.datetime.now())
            resp = requests.get(url, headers=head2, proxies=proxies1, verify=False, timeout=8)
            print resp.text
            if resp.status_code == 200:
                html = resp.text
                soup2 = BeautifulSoup(html, 'lxml')
                return soup2
                break
            elif resp.status_code == 401:
                print u'auth_token 失效,重新获取'
                auth_token = get_auth_token()
                continue
            elif resp.status_code == 403:
                print '该账户需要验证码验证'
                auth_token = get_auth_token()
                continue
            else:
                print '之前没发现的错误 '
                print 'VVVVVVVVVVVVVVVVV'
                print resp.status_code
                print resp.text
                print '^^^^^^^^^^^^^^^^'
                auth_token = get_auth_token()
                break

        except Exception, e:
            print traceback.format_exc()
            if str(e).find('list index out of range') >= 0:
                print u'get cookie 失败 重试'
                continue
            else:
                print str(e)
                break


# ---------------------------------------------------


def get_base_business_info(corp_url, conn, cursor, proxy_list):
    while True:
        try:
            corp_id = corp_url.split('/company/')[1]
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            content_1 = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)
            if content_1 != None:
                if content_1.text.__contains__('https://static.tianyancha.com/wap/images/notFound.png'):
                    print u'nofound.img'
                    break
                elif content_1.text.__contains__('https://m.tianyancha.com/login'):
                    print u'需要登录 重试一次'
                    time.sleep(2)
                    continue
                else:

                    corp_name = \
                        reS_findall(
                            '<title>(.*?)</title>', str(content_1.text))[0].split('_')[0]

                    phone = reS_findall('<span class="sec-c3">电话：</span><span>(.*?)</span>', str(content_1.text))[0]

                    email = reS_findall(
                        '<span class="in-block vertical-top sec-c3.*?">邮箱：</span>(.*?)</div>',
                        str(content_1.text))[0]

                    website = \
                        reS_findall(
                            '<span class="sec-c3">网址：</span>(.*?)</div><div class="in-block vertical-top" style="width: 620px;">',
                            str(content_1.text))[0]

                    address = reS_findall(
                        '<span class="in-block vertical-top sec-c3">地址：</span>(.*?)</div>',
                        str(content_1.text))[0]

                    print corp_url
                    print corp_name
                    print phone
                    print detag(email)
                    print detag(website)
                    print detag(address)

                    if content_1.text.__contains__('法人信息'):
                        juridical_person = \
                            reS_findall("onclick=\"common.stopPropagation\(event\)\">(.*?)</a></div>",
                                        str(content_1.text)
                                        )[0]
                        juridical_person_url = \
                            reS_findall("onclick=\"common.openUrl\('(.*?)'\)\">", str(content_1.text))[0]

                        register_capital = \
                            reS_findall('<div class="pb10">(.*?)</div>', str(content_1.text))[0]
                        real_register_capital = get_code(register_capital, proxies)
                        try:

                            register_time = \
                                reS_findall('<div class="pb10">(.*?)</div>', str(content_1.text))[1]

                            real_register_time = get_code(register_time, proxies)
                        except:
                            real_register_time = 'N'
                        company_status = \
                            reS_findall('<div class="new-c1 pb5">公司状态</div><div><div title="(.*?)"', str(content_1.text)
                                        )[0]

                        business_registration_number = reS_findall('reg-number="(.*?)"', str(content_1.text))[0]

                        uniform_credit_code = \
                            reS_findall('</span></span></span></span></td><td>(.*?)</td><td class="table-left">公司类型',
                                        str(content_1.text))[0]

                        taxpayer_identification_number = reS_findall('纳税人识别号</td><td>(.*?)</td>', str(content_1.text))[
                            0]

                        business_term = reS_findall('营业期限</td><td><span>(.*?)</span>', str(content_1.text))[0]
                        try:
                            registration_authority = \
                                re_findall(
                                    '<span class="sec-cyel">北大法宝</span>提供</span></span></span></span></td><td>(.*?)</td>',
                                    str(content_1.text))[1]
                        except:
                            registration_authority = 'N'
                        registered_address = \
                            re_findall('<td colspan="4">(.*?)<span class="tic tic-fujin c9">', str(content_1.text))[0]
                        try:
                            organization_code = re.findall('<td width="20%">(.*?)</td>', str(content_1.text))[1]
                        except:
                            organization_code = 'N'
                        enterprise_type = \
                            re_findall('<td class="table-left">公司类型</td><td>(.*?)</td></tr>', str(content_1.text))[0]
                        industry = re_findall('<td colspan="2">(.*?)</td>', str(content_1.text))[0]
                        approval_date = re_findall('<text class="tyc-num" >(.*?)</text></td>', str(content_1.text))[0]

                        real_approval_date = get_code(approval_date, proxies)

                        try:
                            english_name = re_findall('<td colspan="2">(.*?)</td>', str(content_1.text))[2]
                        except:
                            english_name = 'N'
                        scope_of_business = \
                            reS_findall(
                                '<span class="js-full-container.*?">(.*?)</span>',
                                str(content_1.text))[0]

                        tyc_num_code = re_findall('<body class="font-(.*?)">', str(content_1.text))[0]
                        tyc_num = tyc_num_code[:2] + '/' + tyc_num_code
                        css_link = 'https://static.tianyancha.com/fonts-styles/css/' + tyc_num + '/font.css'
                        tyc_link = "src: url('https://static.tianyancha.com/fonts-styles/fonts/" + tyc_num + "/tyc-num.eot'); /* IE9*/\nsrc: url('https://static.tianyancha.com/fonts-styles/fonts/" + tyc_num + "/tyc-num.eot#iefix') format('embedded-opentype'), /* IE6-IE8 */\nurl('https://static.tianyancha.com/fonts-styles/fonts/" + tyc_num + "/tyc-num.woff') format('woff'), /* chrome, firefox */\nurl('https://static.tianyancha.com/fonts-styles/fonts/" + tyc_num + "/tyc-num.ttf') format('truetype'), /* chrome, firefox, opera, Safari, Android, iOS 4.2+*/\nurl('https://static.tianyancha.com/fonts-styles/fonts/" + tyc_num + "/tyc-num.svg#tic') format('svg'); /* iOS 4.1- */"

                        print juridical_person
                        print juridical_person_url
                        print real_register_capital
                        print real_register_time
                        print company_status
                        print business_registration_number
                        print organization_code
                        print uniform_credit_code
                        print enterprise_type
                        print taxpayer_identification_number
                        print industry
                        print business_term
                        print approval_date
                        print registration_authority
                        print detag(registered_address)
                        print english_name
                        print detag(scope_of_business)
                        print css_link
                        print tyc_link
                    else:
                        print u'没有工商信息'
                        juridical_person = 'no_data'
                        juridical_person_url = 'no_data'
                        real_register_capital = 'no_data'
                        real_register_time = 'no_data'
                        company_status = 'no_data'
                        business_registration_number = 'no_data'
                        organization_code = 'no_data'
                        uniform_credit_code = 'no_data'
                        enterprise_type = 'no_data'
                        taxpayer_identification_number = 'no_data'
                        industry = 'no_data'
                        business_term = 'no_data'
                        real_approval_date = 'no_data'
                        registration_authority = 'no_data'
                        registered_address = 'no_data'
                        english_name = 'no_data'
                        scope_of_business = 'no_data'
                        css_link = 'no_data'
                        tyc_link = 'no_data'

                    cursor.execute(
                        'insert into tyc_base_business_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (
                            corp_url
                            ,
                            corp_name
                            ,
                            corp_id
                            ,
                            phone
                            ,
                            detag(email)
                            ,
                            detag(website)
                            ,
                            detag(address)
                            ,
                            juridical_person
                            ,
                            juridical_person_url
                            ,
                            real_register_capital
                            ,
                            real_register_time
                            ,
                            company_status
                            ,
                            business_registration_number
                            ,
                            organization_code
                            ,
                            uniform_credit_code
                            ,
                            enterprise_type
                            ,
                            taxpayer_identification_number
                            ,
                            industry
                            ,
                            business_term
                            ,
                            real_approval_date
                            ,
                            registration_authority
                            ,
                            detag(registered_address)
                            ,
                            english_name
                            ,
                            detag(scope_of_business)
                            ,
                            css_link
                            ,
                            tyc_link
                            ,

                            str(datetime.datetime.now()),
                            str(datetime.datetime.now())[:10]
                        )

                    )
                    conn.commit()
                    print corp_name + u'的基本信息获取;插入 ' + str(datetime.datetime.now())

            break
        except Exception as e:
            print corp_url + ' have error :\t%s' % e
            if str(e).__contains__('HTTPSConnectionPool'):
                print 'HTTPSConnectionPool error'
                continue
            else:
                print traceback.format_exc()
                continue


def staff(corp_url, conn, cursor, proxy_list):
    print u'爬取主要人员信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-staffCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-staffCount > span')[0].text
                    print num
                    if int(num) <= 20:

                        res = soup.select('div.staffinfo-module-container')
                        for x in range(len(res)):
                            source = str(res[x])
                            position = re_findall('f9f9f9;"><span>(.*?)</span></div>', source)[0]
                            print detag(position)
                            name = re_findall('target="_blank">(.*?)</a><div class="in-block', source)[0]
                            print name
                            ID = re_findall('"企业详情-主要人员" href="(.*?)"', source)[0]
                            print ID
                            cursor.execute(
                                'insert into tyc_staff_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 detag(position),
                                 name,
                                 ID,

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                        print corp_name + u' 主要人员信息准备就绪  ' + str(datetime.datetime.now())


                    else:
                        auth_token = get_auth_token()

                        if int(num) % 20 == 0:
                            all_page_no = int(num) / 20
                        else:
                            all_page_no = int(num) / 20 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('staff', i, 20, corp_id, auth_token)
                            # print soup2
                            res = soup2.select('div.staffinfo-module-container')
                            for x in range(len(res)):
                                source = str(res[x])
                                print source
                                position = re_findall('f9f9f9;"><span>(.*?)</span></div>', source)[0]
                                print detag(position)
                                name = re_findall('target="_blank">(.*?)</a><div class="in-block', source)[0]
                                print name
                                ID = re_findall('"企业详情-主要人员" href="(.*?)"', source)[0]
                                print ID
                                cursor.execute(
                                    'insert into tyc_staff_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     detag(position),
                                     name,
                                     ID,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 主要人员信息准备就绪  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有主要人员信息  ' + str(datetime.datetime.now())
                    cursor.execute(
                        'insert into tyc_staff_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def shareholder(corp_url, conn, cursor, proxy_list):
    print u'爬取股东信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]
    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                if html.text.__contains__('nav-main-holderCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-holderCount > span')[0].text

                    if int(num) <= 10:
                        res = soup.select('#_container_holder > div > table > tbody > tr')
                        print res
                        for x in range(len(res)):
                            source = str(res[x])

                            shareholder = re.findall('title="(.*?)"', source)[0]
                            ratio = re.findall('<span class="c-money-y">(.*?)</span>', source)[0]
                            value = re.findall('<span class="">(.*?)</span>', source)[0]
                            print shareholder
                            print ratio
                            print value
                            cursor.execute(
                                'insert into tyc_shareholder_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 shareholder,
                                 ratio,
                                 value,

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                        print corp_name + u' 股东信息准备就绪 ' + str(datetime.datetime.now())
                    else:
                        auth_token = get_auth_token()

                        if int(num) % 10 == 0:
                            all_page_no = int(num) / 10
                        else:
                            all_page_no = int(num) / 10 + 1

                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('holder', i, 20, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                shareholder = re.findall('title="(.*?)"', source)[0]
                                ratio = re.findall('<span class="c-money-y">(.*?)</span>', source)[0]
                                value = re.findall('<span class="">(.*?)</span>', source)[0]
                                print shareholder
                                print ratio
                                print value
                                cursor.execute(
                                    'insert into tyc_shareholder_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     shareholder,
                                     ratio,
                                     value,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                                print corp_name + u' 股东信息准备就绪 ' + str(datetime.datetime.now())

                else:
                    print corp_name + u' 没有股东信息 ' + str(datetime.datetime.now())
                    cursor.execute(
                        'insert into tyc_shareholder_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
            break

        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def out_invest(corp_url, conn, cursor, proxy_list):
    print u'爬取对外投资信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-inverst'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-inverstCount > span')[0].text
                    if int(num) <= 20:
                        res = soup.select('#_container_invest > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            invested_person = re_findall('title="(.*?)"', source)
                            span_part = re_findall('<span class.*?>(.*?)</span>', source)
                            print invested_person[0]
                            print span_part[0]
                            print span_part[2]
                            print span_part[3]
                            print span_part[4]
                            print span_part[5]
                            print span_part[6]
                            cursor.execute(
                                'insert tyc_out_investment_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 span_part[0].decode('utf-8'),
                                 invested_person[0].decode('utf-8'),
                                 span_part[2].decode('utf-8'),
                                 span_part[3].decode('utf-8'),
                                 span_part[4].decode('utf-8'),
                                 span_part[5].decode('utf-8'),
                                 span_part[6].decode('utf-8'),
                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                        print corp_name + u' 对外投资信息准备就绪  ' + str(datetime.datetime.now())

                    else:
                        auth_token = get_auth_token()
                        if int(num) % 20 == 0:
                            all_page_no = int(num) / 20
                        else:
                            all_page_no = int(num) / 20 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('invest', i, 20, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                invested_person = re_findall('title="(.*?)"', source)
                                span_part = re_findall('<span class.*?>(.*?)</span>', source)
                                cursor.execute(
                                    'insert tyc_out_investment_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     span_part[0].decode('utf-8'),
                                     invested_person[0].decode('utf-8'),
                                     span_part[2].decode('utf-8'),
                                     span_part[3].decode('utf-8'),
                                     span_part[4].decode('utf-8'),
                                     span_part[5].decode('utf-8'),
                                     span_part[6].decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 对外投资信息准备就绪  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有对外投资信息  ' + str(datetime.datetime.now())
                    cursor.execute(
                        'insert tyc_out_investment_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def change_info(corp_url, conn, cursor, proxy_list):
    print u'爬取变更记录信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-changeCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-changeCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_changeinfo > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            part_one = re_findall('<div>(.*?)</div>', source)
                            part_two = re_findall('<div class="textJustFy changeHoverText.*?">(.*?)</div>', source)
                            change_time = part_one[0]
                            change_projects = part_one[1]
                            before_change = detag(part_two[0])
                            after_change = detag(part_two[1])
                            print change_time.decode('utf-8')
                            print change_projects.decode('utf-8')
                            print before_change.decode('utf-8')
                            print after_change.decode('utf-8')
                            cursor.execute(
                                'insert into tyc_change_record_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,
                                 change_time.decode('utf-8'),
                                 change_projects.decode('utf-8'),
                                 detag(before_change).decode('utf-8'),
                                 detag(after_change).decode('utf-8'),
                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                        print corp_name + u' 变更信息准备就绪  ' + str(datetime.datetime.now())
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('changeinfo', i, 20, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                part_one = re_findall('<div>(.*?)</div>', source)
                                part_two = re_findall('<div class="textJustFy changeHoverText.*?">(.*?)</div>', source)
                                change_time = part_one[0]
                                change_projects = part_one[1]
                                before_change = detag(part_two[0])
                                after_change = detag(part_two[1])
                                print change_time.decode('utf-8')
                                print change_projects.decode('utf-8')
                                print before_change.decode('utf-8')
                                print after_change.decode('utf-8')
                                cursor.execute(
                                    'insert into tyc_change_record_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,
                                     change_time.decode('utf-8'),
                                     change_projects.decode('utf-8'),
                                     detag(before_change).decode('utf-8'),
                                     detag(after_change).decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 变更信息准备就绪  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有变更信息 ' + str(datetime.datetime.now())
                    cursor.execute(
                        'insert into tyc_change_record_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def branch(corp_url, conn, cursor, proxy_list):
    print u'爬取分支机构信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-branchCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-branchCount > span')[0].text
                    if int(num) <= 10:
                        res = soup.select('#_container_branch > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            print source
                            branch_name = re.findall('<span>(.*?)</span></a>', source)[0]
                            branch_url = re.findall('<a class="query_name" href="(.*?)" style=', source)[0]
                            legal_representative = re.findall('</a></td><td><span>(.*?)</span>', source)[0]
                            state = re.findall('<span class=".*?">(.*?)</span></td><td>', source)[0]
                            try:
                                register_time = re.findall('<span class="">(.*?)</span></td></tr>', source)[0]
                            except:
                                register_time = ''
                            print branch_name
                            print branch_url
                            print legal_representative
                            print state
                            print register_time
                            cursor.execute(
                                'insert into tyc_branch_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 branch_name,
                                 branch_url,
                                 legal_representative,
                                 state,
                                 register_time,

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                        print corp_name + u' 分支机构插入成功  ' + str(datetime.datetime.now())
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 10 == 0:
                            all_page_no = int(num) / 10
                        else:
                            all_page_no = int(num) / 10 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('branch', i, 10, corp_id, auth_token)
                            print soup2
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source
                                branch_name = re.findall('<span>(.*?)</span></a>', source)[0]
                                branch_url = re.findall('<a class="query_name" href="(.*?)" style=', source)[0]
                                legal_representative = re.findall('</a></td><td><span>(.*?)</span>', source)[0]
                                state = re.findall('<span class=".*?">(.*?)</span></td><td>', source)[0]
                                try:
                                    register_time = re.findall('<span class="">(.*?)</span></td></tr>', source)[0]
                                except:
                                    register_time = ''
                                print branch_name

                                print branch_url
                                print legal_representative
                                print state
                                print register_time
                                cursor.execute(
                                    'insert into tyc_branch_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     branch_name,
                                     branch_url,
                                     legal_representative,
                                     state,
                                     register_time,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' 分支机构插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有分支机构信息  ' + str(datetime.datetime.now())
                    cursor.execute(
                        'insert into tyc_branch_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no data',
                         'no data',
                         'no data',
                         'no data',
                         'no data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()

            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def rongziHistory(corp_url, conn, cursor, proxy_list):
    print u'获取融资历史  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-companyRongzi'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-companyRongzi > span')[0].text
                    if int(num) <= 10:
                        res = soup.select('#_container_rongzi > div > div > table > tbody > tr')

                        for x in range(len(res)):
                            source = str(res[x])
                            all_1 = re_findall('<td><span class="text-dark-color .*?">(.*?)</span></td>', source)
                            asd = re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)
                            cursor.execute(
                                'insert into tyc_rongziCompany_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (
                                    corp_id,
                                    corp_name,
                                    corp_url,

                                    all_1[0].decode('utf-8'),
                                    all_1[1].decode('utf-8'),
                                    all_1[2].decode('utf-8'),
                                    all_1[3].decode('utf-8'),
                                    all_1[4].decode('utf-8'),
                                    detag(asd[0]).decode('utf-8'),
                                    detag(asd[1]).decode('utf-8'),

                                    str(datetime.datetime.now()),
                                    str(datetime.datetime.now())[:10])
                            )
                            conn.commit()

                        print u'融资历史插入成功 ok !!! @__ ' + str(datetime.datetime.now())

                    else:
                        auth_token = get_auth_token()
                        if int(num) % 10 == 0:
                            all_page_no = int(num) / 10
                        else:
                            all_page_no = int(num) / 10 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('rongzi', i, 10, corp_name, auth_token)
                            res = soup2.select('tr')

                            for x in range(1, len(res)):
                                source = str(res[x])
                                # print source
                                all_1 = re_findall('<td><span class="text-dark-color .*?">(.*?)</span></td>', source)
                                # print all_1[0]
                                # print all_1[1]
                                # print all_1[2]
                                # print all_1[3]
                                # print all_1[4]

                                asd = re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)
                                # print detag(asd[0])
                                # print detag(asd[1])
                                cursor.execute(
                                    'insert into tyc_rongziCompany_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (
                                        corp_id,
                                        corp_name,
                                        corp_url,

                                        all_1[0].decode('utf-8'),
                                        all_1[1].decode('utf-8'),
                                        all_1[2].decode('utf-8'),
                                        all_1[3].decode('utf-8'),
                                        all_1[4].decode('utf-8'),
                                        detag(asd[0]).decode('utf-8'),
                                        detag(asd[1]).decode('utf-8'),

                                        str(datetime.datetime.now()),
                                        str(datetime.datetime.now())[:10])
                                )
                                conn.commit()

                        print corp_name + u' 融资历史信息准备就绪 ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有融资历史信息 ' + str(datetime.datetime.now())

                    cursor.execute(
                        'insert into tyc_rongziCompany_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (
                            corp_id,
                            corp_name,
                            corp_url,

                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',

                            str(datetime.datetime.now()),
                            str(datetime.datetime.now())[:10])
                    )
            break
        except Exception, e:

            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def core_team(corp_url, conn, cursor, proxy_list):
    print u'爬取核心团队信息 ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue

            else:

                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-companyTeammember'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-companyTeammember > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('div.team-item')
                        for x in range(len(res)):
                            source = str(res[x])
                            name = re.findall('img alt="(.*)" src', source)[0]
                            icon = re.findall('src="(.*?)"', source)[0]
                            position = re.findall('<div class="team-title">(.*?)</div><ul>', source)[0]
                            brief = \
                                re.findall('<span class="text-dark-color">(.*?)</span></li></ul></div></div>',
                                           source)[
                                    0]
                            print name
                            print icon
                            print detag(position)
                            print detag(brief)
                            cursor.execute(
                                'insert into tyc_core_team_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 icon,
                                 name,
                                 detag(position),
                                 detag(brief),

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                        print corp_name + u' - 核心团队信息准备就绪 ' + str(datetime.datetime.now())

                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('teamMember', i, 5, corp_name, auth_token)

                            res = soup2.select('div.team-item')
                            for x in range(len(res)):
                                source = str(res[x])
                                name = re.findall('img alt="(.*)" src', source)[0]
                                icon = re.findall('src="(.*?)"', source)[0]
                                position = re.findall('<div class="team-title">(.*?)</div><ul>', source)[0]
                                brief = \
                                    re.findall('<span class="text-dark-color">(.*?)</span></li></ul></div></div>',
                                               source)[
                                        0]
                                print name
                                print icon
                                print detag(position)
                                print detag(brief)

                                cursor.execute(
                                    'insert into tyc_core_team_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     icon,
                                     name,
                                     detag(position),
                                     detag(brief),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            print corp_name + u' - 核心团队信息准备就绪 ' + str(datetime.datetime.now())

                else:
                    print u'没有核心团队信息'
                    cursor.execute(
                        'insert into tyc_core_team_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()

            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def companyProduct(corp_url, conn, cursor, proxy_list):
    print u'获取企业业务部分  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-companyProduct'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-companyProduct > span')[0].text
                    if int(num) <= 15:
                        res = soup.select('div[class="product-item"]')
                        for x in range(len(res)):
                            source = str(res[x])
                            name = re_findall('img alt="(.*?)"', source)[0]
                            img = re_findall('src="(.*?)"', source)[0]
                            category = detag(re_findall('<div class=".*?">(.*?)</div>', source)[2])
                            brief = detag(re_findall('<div class=".*?">(.*?)</div>', source)[3])

                            print name
                            print img
                            print category
                            print brief
                            cursor.execute(
                                'insert into tyc_companyProduct_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 name.decode('utf-8'),
                                 img.decode('utf-8'),
                                 category.decode('utf-8'),
                                 brief.decode('utf-8'),

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                        print corp_name + u' 企业业务信息插入成功  ' + str(datetime.datetime.now())

                    else:
                        auth_token = get_auth_token()

                        if int(num) % 15 == 0:
                            all_page_no = int(num) / 15
                        else:
                            all_page_no = int(num) / 15 + 1

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('firmProduct', i, 15, corp_name, auth_token)
                            res = soup2.select('div[class="product-item"]')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                name = re_findall('img alt="(.*?)"', source)[0]
                                img = re_findall('src="(.*?)"', source)[0]
                                category = detag(re_findall('<div class=".*?">(.*?)</div>', source)[2])
                                brief = detag(re_findall('<div class=".*?">(.*?)</div>', source)[3])

                                print name
                                print img
                                print category
                                print brief
                                cursor.execute(
                                    'insert into tyc_companyProduct_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     name.decode('utf-8'),
                                     img.decode('utf-8'),
                                     category.decode('utf-8'),
                                     brief.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                        print corp_name + u' 企业业务信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有企业业务信息 ' + str(datetime.datetime.now())

                    cursor.execute(
                        'insert into tyc_companyProduct_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def investment_info(corp_url, conn, cursor, proxy_list):
    print u'获取投资事件部分  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-jigouTzanli'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-jigouTzanli > span')[0].text
                    if int(num) <= 10:
                        res = soup.select('#_container_touzi > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])

                            date = re_findall('<span class=".*?">(.*?)</span>', source)[0]
                            round = re_findall('<span class=".*?">(.*?)</span>', source)[1]
                            amount = re_findall('<span class=".*?">(.*?)</span>', source)[2]
                            investor = detag(re_findall('<div>(.*?)<img alt', source)[0])
                            product_name = re_findall('img alt="(.*?)"', source)[0]
                            product_icon = re_findall('src="(.*?)"', source)[0]
                            location = re_findall('<span class=".*?">(.*?)</span>', source)[3]
                            industry = re_findall('<span class=".*?">(.*?)</span>', source)[4]
                            business = re_findall('<span class=".*?">(.*?)</span>', source)[5]

                            print date
                            print round
                            print amount
                            print investor
                            print product_name
                            print product_icon
                            print location
                            print industry
                            print business
                            cursor.execute(
                                'insert into tyc_investment_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 date.decode('utf-8'),
                                 round.decode('utf-8'),
                                 amount.decode('utf-8'),
                                 investor.decode('utf-8'),
                                 product_name.decode('utf-8'),
                                 product_icon.decode('utf-8'),
                                 location.decode('utf-8'),
                                 industry.decode('utf-8'),
                                 business.decode('utf-8'),

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()

                        print corp_name + u' 投资事件部分信息插入成功  ' + str(datetime.datetime.now())
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 10 == 0:
                            all_page_no = int(num) / 10
                        else:
                            all_page_no = int(num) / 10 + 1
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('touzi', i, 10, corp_name, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                date = re_findall('<span class=".*?">(.*?)</span>', source)[0]
                                round = re_findall('<span class=".*?">(.*?)</span>', source)[1]
                                amount = re_findall('<span class=".*?">(.*?)</span>', source)[2]
                                investor = detag(re_findall('<div>(.*?)<img alt', source)[0])
                                product_name = re_findall('img alt="(.*?)"', source)[0]
                                product_icon = re_findall('src="(.*?)"', source)[0]
                                location = re_findall('<span class=".*?">(.*?)</span>', source)[3]
                                industry = re_findall('<span class=".*?">(.*?)</span>', source)[4]
                                business = re_findall('<span class=".*?">(.*?)</span>', source)[5]

                                # print date
                                # print round
                                # print amount
                                # print investor
                                # print product_name
                                # print product_icon
                                # print location
                                # print industry
                                # print business
                                cursor.execute(
                                    'insert into tyc_investment_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     date.decode('utf-8'),
                                     round.decode('utf-8'),
                                     amount.decode('utf-8'),
                                     investor.decode('utf-8'),
                                     product_name.decode('utf-8'),
                                     product_icon.decode('utf-8'),
                                     location.decode('utf-8'),
                                     industry.decode('utf-8'),
                                     business.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()

                        print corp_name + u' 投资事件部分信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有投资事件部分信息 ' + str(datetime.datetime.now())

                    cursor.execute(
                        'insert into tyc_investment_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue

            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()

                break


def jingpin(corp_url, conn, cursor, proxy_list):
    print u'获取竞品信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-companyJingpin'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-companyJingpin > span')[0].text
                    if int(num) <= 10:
                        res = soup.select('#_container_jingpin > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            print source
                            if source.find('href') >= 0:
                                icon = re_findall('src="(.*?)"', source)[0]
                                name = re_findall('img alt="(.*?)"', source)[0]
                                jingpin_ID = re_findall('href="https://www.tianyancha.com/company/(.*?)"', source)[
                                    0]

                                location = re_findall('span class=".*?">(.*?)</span>', source)[0]
                                current_round = re_findall('span class=".*?">(.*?)</span>', source)[1]
                                industry = re_findall('span class=".*?">(.*?)</span>', source)[2]
                                business = re_findall('span class=".*?">(.*?)</span>', source)[3]
                                create_time = re_findall('span class=".*?">(.*?)</span>', source)[4]
                                valuation = re_findall('span class=".*?">(.*?)</span>', source)[5]
                            else:
                                icon = re_findall('src="(.*?)"', source)[0]
                                name = re_findall('img alt="(.*?)"', source)[0]
                                jingpin_ID = u'not found'
                                location = re_findall('span class=".*?">(.*?)</span>', source)[1]
                                current_round = re_findall('span class=".*?">(.*?)</span>', source)[2]
                                industry = re_findall('span class=".*?">(.*?)</span>', source)[3]
                                business = re_findall('span class=".*?">(.*?)</span>', source)[4]
                                create_time = re_findall('span class=".*?">(.*?)</span>', source)[5]
                                valuation = re_findall('span class=".*?">(.*?)</span>', source)[6]
                            cursor.execute(
                                'insert into tyc_jingpin_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (
                                    corp_id,
                                    corp_name,
                                    corp_url,
                                    icon.decode('utf-8'),
                                    name.decode('utf-8'),
                                    jingpin_ID,
                                    location.decode('utf-8'),
                                    current_round.decode('utf-8'),
                                    industry.decode('utf-8'),
                                    business.decode('utf-8'),
                                    create_time.decode('utf-8'),
                                    valuation.decode('utf-8'),
                                    str(datetime.datetime.now()),
                                    str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()

                        if int(num) % 10 == 0:
                            all_page_no = int(num) / 10
                        else:
                            all_page_no = int(num) / 10 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('jingpin', i, 10, corp_name, auth_token)
                            res = soup2.select('tr')

                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source
                                if source.find('href') >= 0:
                                    icon = re_findall('src="(.*?)"', source)[0]
                                    name = re_findall('img alt="(.*?)"', source)[0]
                                    jingpin_ID = re_findall('href="https://www.tianyancha.com/company/(.*?)"', source)[
                                        0]

                                    location = re_findall('span class=".*?">(.*?)</span>', source)[0]
                                    current_round = re_findall('span class=".*?">(.*?)</span>', source)[1]
                                    industry = re_findall('span class=".*?">(.*?)</span>', source)[2]
                                    business = re_findall('span class=".*?">(.*?)</span>', source)[3]
                                    create_time = re_findall('span class=".*?">(.*?)</span>', source)[4]
                                    valuation = re_findall('span class=".*?">(.*?)</span>', source)[5]
                                else:
                                    icon = re_findall('src="(.*?)"', source)[0]
                                    name = re_findall('img alt="(.*?)"', source)[0]
                                    jingpin_ID = u'not found'
                                    location = re_findall('span class=".*?">(.*?)</span>', source)[1]
                                    current_round = re_findall('span class=".*?">(.*?)</span>', source)[2]
                                    industry = re_findall('span class=".*?">(.*?)</span>', source)[3]
                                    business = re_findall('span class=".*?">(.*?)</span>', source)[4]
                                    create_time = re_findall('span class=".*?">(.*?)</span>', source)[5]
                                    valuation = re_findall('span class=".*?">(.*?)</span>', source)[6]

                                cursor.execute(
                                    'insert into tyc_jingpin_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (
                                        corp_id,
                                        corp_name,
                                        corp_url,
                                        icon.decode('utf-8'),
                                        name.decode('utf-8'),
                                        jingpin_ID,
                                        location.decode('utf-8'),
                                        current_round.decode('utf-8'),
                                        industry.decode('utf-8'),
                                        business.decode('utf-8'),
                                        create_time.decode('utf-8'),
                                        valuation.decode('utf-8'),
                                        str(datetime.datetime.now()),
                                        str(datetime.datetime.now())[:10])
                                )
                                conn.commit()

                    print corp_name + u' 竞品信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有竞品信息 ' + str(datetime.datetime.now())
                    cursor.execute(
                        'insert into tyc_jingpin_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (
                            corp_id,
                            corp_name,
                            corp_url,

                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',

                            str(datetime.datetime.now()),
                            str(datetime.datetime.now())[:10])
                    )

                break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def lawsuit(corp_url, conn, cursor, proxy_list):
    print u'爬取法律诉讼信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-lawsuitCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-lawsuitCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_lawsuit > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])

                            date = re_findall('<span class=".*?">(.*?)</span>', source)[0]
                            Judgment_document_url = re_findall('href="(.*?)" href-new-event', source)[0]
                            Judgment_document_name = re_findall('target="_blank">(.*?)</a>', source)[0]
                            cause = re_findall('<span class=".*?">(.*?)</span>', source)[1]
                            identity = detag(re_findall('<div class="text-dark-color">(.*?)</div>', source)[0])
                            docket_number = re_findall('<span class=".*?">(.*?)</span>', source)[2]
                            cursor.execute(
                                'insert into tyc_lawsuit_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 date.decode('utf-8'),
                                 Judgment_document_name.decode('utf-8'),
                                 Judgment_document_url.decode('utf-8'),
                                 cause.decode('utf-8'),
                                 identity.decode('utf-8'),
                                 docket_number.decode('utf-8'),

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10]
                                 )
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()

                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('lawsuit', i, 5, corp_name, auth_token)

                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source

                                date = re_findall('<span class=".*?">(.*?)</span>', source)[0]
                                Judgment_document_url = re_findall('href="(.*?)" href-new-event', source)[0]
                                Judgment_document_name = re_findall('target="_blank">(.*?)</a>', source)[0]
                                cause = re_findall('<span class=".*?">(.*?)</span>', source)[1]
                                identity = detag(re_findall('<div class="text-dark-color">(.*?)</div>', source)[0])
                                docket_number = re_findall('<span class=".*?">(.*?)</span>', source)[2]
                                cursor.execute(
                                    'insert into tyc_lawsuit_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     date.decode('utf-8'),
                                     Judgment_document_name.decode('utf-8'),
                                     Judgment_document_url.decode('utf-8'),
                                     cause.decode('utf-8'),
                                     identity.decode('utf-8'),
                                     docket_number.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10]
                                     )
                                )
                                conn.commit()
                    print corp_name + u' 法律诉讼信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有法律诉讼信息 ' + str(datetime.datetime.now())

                    cursor.execute(
                        'insert into tyc_lawsuit_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10]
                         )
                    )

            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def court(corp_url, conn, cursor, proxy_list):
    print u'爬取法院公告信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-courtCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-courtCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_court > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])

                            date = re_findall('"publishdate":"(.*?)"', source)[0]
                            appellant = re_findall('"party1":"(.*?)"', source)[0]
                            defendant = re_findall('"party2":"(.*?)"', source)[0]
                            type = re_findall('"bltntypename":"(.*?)"', source)[0]
                            court = re_findall('"courtcode":"(.*?)"', source)[0]
                            detail = re_findall('"content":"(.*?)"', source)[0]
                            cursor.execute(
                                'insert into tyc_court_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,
                                 date.decode('utf-8'),
                                 appellant.decode('utf-8'),
                                 defendant.decode('utf-8'),
                                 type.decode('utf-8'),
                                 court.decode('utf-8'),
                                 detail.decode('utf-8'),
                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10]
                                 )
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('court', i, 5, corp_name, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                date = re_findall('"publishdate":"(.*?)"', source)[0]
                                appellant = re_findall('"party1":"(.*?)"', source)[0]
                                defendant = re_findall('"party2":"(.*?)"', source)[0]
                                type = re_findall('"bltntypename":"(.*?)"', source)[0]
                                court = re_findall('"courtcode":"(.*?)"', source)[0]
                                detail = re_findall('"content":"(.*?)"', source)[0]
                                cursor.execute(
                                    'insert into tyc_court_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,
                                     date.decode('utf-8'),
                                     appellant.decode('utf-8'),
                                     defendant.decode('utf-8'),
                                     type.decode('utf-8'),
                                     court.decode('utf-8'),
                                     detail.decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10]
                                     )
                                )
                                conn.commit()
                    print corp_name + u' 法院公告信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有法院公告信息 ' + str(datetime.datetime.now())

                    cursor.execute(
                        'insert into tyc_court_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10]
                         )
                    )

                    conn.commit()
            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def dishonest(corp_url, conn, cursor, proxy_list):
    print u'爬取失信人信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-dishonest'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-dishonest > span')[0].text
                    # for i in range(1, int(num) + 1):
                    #     date = soup.select(
                    #         '#web-content > div > div > div.container.company_container > div > div.col-9.company-main.pl0.pr10.company_new_2017 > div > div.pl30.pr30.pt25 > div:nth-of-type(14) > div:nth-of-type(2) > div > table > tbody > tr:nth-of-type(' + str(
                    #             i) + ') > td:nth-of-type(1) > span')[0].text
                    #     case_no = soup.select(
                    #         '#web-content > div > div > div.container.company_container > div > div.col-9.company-main.pl0.pr10.company_new_2017 > div > div.pl30.pr30.pt25 > div:nth-of-type(14) > div:nth-of-type(2) > div > table > tbody > tr:nth-of-type(' + str(
                    #             i) + ') > td:nth-of-type(2) > span')[0].text
                    #     court = soup.select(
                    #         '#web-content > div > div > div.container.company_container > div > div.col-9.company-main.pl0.pr10.company_new_2017 > div > div.pl30.pr30.pt25 > div:nth-of-type(14) > div:nth-of-type(2) > div > table > tbody > tr:nth-of-type(' + str(
                    #             i) + ') > td:nth-of-type(3) > span')[0].text
                    #     state = soup.select(
                    #         '#web-content > div > div > div.container.company_container > div > div.col-9.company-main.pl0.pr10.company_new_2017 > div > div.pl30.pr30.pt25 > div:nth-of-type(14) > div:nth-of-type(2) > div > table > tbody > tr:nth-of-type(' + str(
                    #             i) + ') > td:nth-of-type(4) > span')[0].text
                    #     reference = soup.select(
                    #         '#web-content > div > div > div.container.company_container > div > div.col-9.company-main.pl0.pr10.company_new_2017 > div > div.pl30.pr30.pt25 > div:nth-of-type(14) > div:nth-of-type(2) > div > table > tbody > tr:nth-of-type(' + str(
                    #             i) + ') > td:nth-of-type(5) > span')[0].text
                    #     # print date
                    #     # print case_no
                    #     # print court
                    #     # print state
                    #     # print reference
                    if int(num) <= 5:
                        res = soup.select('#_container_dishonest > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            date = re_findall('<span.*?>(.*?)</span>', source)[0]
                            case_no = re_findall('<span.*?>(.*?)</span>', source)[1]
                            court = re_findall('<span.*?>(.*?)</span>', source)[2]
                            state = re_findall('<span.*?>(.*?)</span>', source)[3]
                            reference = re_findall('<span.*?>(.*?)</span>', source)[4]

                            cursor.execute(
                                'insert into tyc_dishonest_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' % (
                                    corp_url,
                                    corp_name,
                                    corp_id,
                                    date,
                                    case_no,
                                    court,
                                    state,
                                    reference,
                                    str(datetime.datetime.now()),
                                    str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('dishonest', i, 5, corp_name, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                date = re_findall('<span.*?>(.*?)</span>', source)[0]
                                case_no = re_findall('<span.*?>(.*?)</span>', source)[1]
                                court = re_findall('<span.*?>(.*?)</span>', source)[2]
                                state = re_findall('<span.*?>(.*?)</span>', source)[3]
                                reference = re_findall('<span.*?>(.*?)</span>', source)[4]

                                cursor.execute(
                                    'insert into tyc_dishonest_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' % (
                                        corp_url,
                                        corp_name,
                                        corp_id,
                                        date,
                                        case_no,
                                        court,
                                        state,
                                        reference,
                                        str(datetime.datetime.now()),
                                        str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' 失信人信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有失信人信息 ' + str(datetime.datetime.now())

                    cursor.execute(
                        'insert into tyc_dishonest_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' % (
                            corp_url,
                            corp_name,
                            corp_id,
                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',
                            'no_data',
                            str(datetime.datetime.now()),
                            str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def zhixing(corp_url, conn, cursor, proxy_list):
    print u'爬取执行信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-zhixing'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-zhixing > span')[0].text

                    if int(num) <= 5:
                        res = soup.select('#_container_zhixing > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            part = re_findall('<span class=".*?">(.*?)</span>', source)
                            cursor.execute(
                                'insert into tyc_zhixing_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,
                                 part[0].decode('utf-8'),
                                 part[1].decode('utf-8'),
                                 part[2].decode('utf-8'),
                                 part[3].decode('utf-8'),
                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10]
                                 )
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('zhixing', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                part = re_findall('<span class=".*?">(.*?)</span>', source)
                                # print part[0]
                                # print part[1]
                                # print part[2]
                                # print part[3]
                                cursor.execute(
                                    'insert into tyc_zhixing_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,
                                     part[0].decode('utf-8'),
                                     part[1].decode('utf-8'),
                                     part[2].decode('utf-8'),
                                     part[3].decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10]
                                     )
                                )
                                conn.commit()
                    print corp_name + u' 执行信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有执行信息  ' + str(datetime.datetime.now())
                    cursor.execute(
                        'insert into tyc_zhixing_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10]
                         )
                    )
                    conn.commit()
            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def announcement(corp_url, conn, cursor, proxy_list):
    print u'爬取开庭公告信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)
            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                # print html.text
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-announcementCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-announcementCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_announcementcourt > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            date = re_findall('<td>(.*?)</td>', source)[0]
                            type = re_findall('<span class="text-dark-color">(.*?)</span>', source)[0]
                            appellant = re_findall('"name":"(.*?)",', source)[0]
                            defendant = re_findall('"name":"(.*?)",', source)[1]
                            case_no = re_findall('"caseNo":"(.*?)",', source)[0]

                            cursor.execute(
                                'insert into tyc_announcement_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,
                                 date.decode('utf-8'),
                                 type.decode('utf-8'),
                                 appellant.decode('utf-8'),
                                 defendant.decode('utf-8'),
                                 case_no.decode('utf-8'),
                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()

                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('announcementcourt', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                date = re_findall('<td>(.*?)</td>', source)[0]
                                type = re_findall('<span class="text-dark-color">(.*?)</span>', source)[0]
                                appellant = re_findall('"name":"(.*?)",', source)[0]
                                defendant = re_findall('"name":"(.*?)",', source)[1]
                                case_no = re_findall('"caseNo":"(.*?)",', source)[0]

                                cursor.execute(
                                    'insert into tyc_announcement_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,
                                     date.decode('utf-8'),
                                     type.decode('utf-8'),
                                     appellant.decode('utf-8'),
                                     defendant.decode('utf-8'),
                                     case_no.decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' 开庭公告信息插入成功  ' + str(datetime.datetime.now())

                    break
                else:
                    print corp_name + u' 没有开庭公告信息  ' + str(datetime.datetime.now())
                    cursor.execute(
                        'insert into tyc_announcement_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
                    break
            break

        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def abnormal(corp_url, conn, cursor, proxy_list):
    print u'爬取经营异常信息 ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)
            # print html.text
            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'

                continue

            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-abnormalCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-abnormalCount > span')[0].text

                    if int(num) <= 5:
                        res = soup.select('#_container_abnormal > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            table_len = str(
                                len(soup.select('#_container_abnormal > div > div > table > thead > tr > th')))
                            print table_len
                            if table_len == '3':
                                print u'只有3个'
                                record_time = re_findall('<td><span>(.*?)</span></td>', source)[0]
                                record_cause = \
                                    re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[0]
                                record_department = \
                                    re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[1]
                                cursor.execute(
                                    'insert into tyc_abnormal_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     record_time,
                                     record_cause,
                                     record_department,
                                     'no_data',
                                     'no_data',
                                     'no_data',

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            elif table_len == '6':
                                print u'只有6个'
                                record_time = re_findall('<td><span>(.*?)</span></td>', source)[0]
                                record_cause = \
                                    re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[0]
                                record_department = \
                                    re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[1]
                                removal_time = re_findall('<td><span>(.*?)</span></td>', source)[1]
                                removal_cause = \
                                    re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[2]
                                removal_department = \
                                    re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[3]

                                cursor.execute(
                                    'insert into tyc_abnormal_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     record_time,
                                     record_cause,
                                     record_department,
                                     removal_time,
                                     removal_cause,
                                     removal_department,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                            else:
                                pass
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1

                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('abnormal', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                table_len = str(len(soup2.select('tr > td')))
                                if table_len == '3':
                                    print u'只有3个'
                                    record_time = re_findall('<td><span>(.*?)</span></td>', source)[0]
                                    record_cause = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[0]
                                    record_department = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[1]
                                    cursor.execute(
                                        'insert into tyc_abnormal_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                        (corp_url,
                                         corp_name,
                                         corp_id,

                                         record_time,
                                         record_cause,
                                         record_department,
                                         'no_data',
                                         'no_data',
                                         'no_data',

                                         str(datetime.datetime.now()),
                                         str(datetime.datetime.now())[:10])
                                    )
                                    conn.commit()
                                elif table_len == '6':
                                    print u'只有6个'
                                    record_time = re_findall('<td><span>(.*?)</span></td>', source)[0]
                                    record_cause = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[0]
                                    record_department = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[1]
                                    removal_time = re_findall('<td><span>(.*?)</span></td>', source)[1]
                                    removal_cause = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[2]
                                    removal_department = \
                                        re_findall('<td><span class="text-dark-color">(.*?)</span></td>', source)[3]

                                    cursor.execute(
                                        'insert into tyc_abnormal_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                        (corp_url,
                                         corp_name,
                                         corp_id,

                                         record_time,
                                         record_cause,
                                         record_department,
                                         removal_time,
                                         removal_cause,
                                         removal_department,

                                         str(datetime.datetime.now()),
                                         str(datetime.datetime.now())[:10])
                                    )
                                    conn.commit()
                                else:
                                    pass
                    print corp_name + u' - 经营异常信息插入成功  ' + str(datetime.datetime.now())

                else:
                    print u'没有经营异常信息'
                    cursor.execute(
                        'insert into tyc_abnormal_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()

            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def punish(corp_url, conn, cursor, proxy_list):
    print u'爬取惩罚信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)
            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-punishment'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-punishment > span')[0].text

                    if int(num) <= 5:
                        res = soup.select('#_container_punish > div > div > table > tbody > tr')

                        for x in range(len(res)):
                            source = str(res[x])
                            date = re_findall('<span class=".*?">(.*?)</span>', source)[0]
                            document_no = re_findall('<span class=".*?">(.*?)</span>', source)[1]
                            type = re_findall('<span class=".*?">(.*?)</span>', source)[2]
                            decision_organ = re_findall('<div class=".*?">(.*?)</div>', source)[0]

                            print date
                            print document_no
                            print type
                            print decision_organ
                            cursor.execute(
                                'insert into tyc_punish_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,
                                 date.decode('utf-8'),
                                 document_no.decode('utf-8'),
                                 type.decode('utf-8'),
                                 decision_organ.decode('utf-8'),
                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()

                    else:
                        auth_token = get_auth_token()

                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_name('punish', i, 5, corp_name, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                print source

                                date = re_findall('<span class=".*?">(.*?)</span>', source)[0]
                                document_no = re_findall('<span class=".*?">(.*?)</span>', source)[1]
                                type = re_findall('<span class=".*?">(.*?)</span>', source)[2]
                                decision_organ = re_findall('<div class=".*?">(.*?)</div>', source)[0]

                                print date
                                print document_no
                                print type
                                print decision_organ
                                cursor.execute(
                                    'insert into tyc_punish_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,
                                     date.decode('utf-8'),
                                     document_no.decode('utf-8'),
                                     type.decode('utf-8'),
                                     decision_organ.decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' 惩罚信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有惩罚信息 ' + str(datetime.datetime.now())
                    cursor.execute(
                        'insert into tyc_punish_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()

            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue

            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def illegal(corp_url, conn, cursor, proxy_list):
    print u'爬取严重违法信息 ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)
            # print html.text
            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'

                continue

            else:

                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name

                if html.text.__contains__('nav-main-illegalCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-illegalCount > span')[0].text

                    if int(num) <= 5:
                        res = soup.select('#_container_illegal > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            record_time = re_findall('<td><span class="">(.*?)</span></td>', source)[0]
                            record_cause = \
                                re_findall('<td><span class="">(.*?)</span></td>', source)[1]
                            record_department = \
                                re_findall('<td><span class="">(.*?)</span></td>', source)[2]
                            cursor.execute(
                                'insert into tyc_illegal_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 record_time,
                                 record_cause,
                                 record_department,

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('illegal', i, 5, corp_name, auth_token)
                            # print soup2
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                record_time = re_findall('<td><span class="">(.*?)</span></td>', source)[0]
                                record_cause = \
                                    re_findall('<td><span class="">(.*?)</span></td>', source)[1]
                                record_department = \
                                    re_findall('<td><span class="">(.*?)</span></td>', source)[2]

                                # print record_time
                                # print record_cause
                                # print record_department

                                cursor.execute(
                                    'insert into tyc_illegal_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     record_time,
                                     record_cause,
                                     record_department,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' - 严重违法信息插入成功  ' + str(datetime.datetime.now())

                else:
                    print u'没有严重违法信息'
                    cursor.execute(
                        'insert into tyc_illegal_info values ("%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()

                break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def equity(corp_url, conn, cursor, proxy_list):
    print u'爬取股权出质信息 ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'

                continue

            else:

                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name

                if html.text.__contains__('nav-main-equityCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-equityCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_equity > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            announcement_time = \
                                re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[0]
                            register_no = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[1]
                            pledgor = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[2]
                            pledgee = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[3]
                            state = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[4]
                            # print announcement_time
                            # print register_no
                            # print detag(pledgor)
                            # print detag(pledgee)
                            # print state

                            cursor.execute(
                                'insert into tyc_equity_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 announcement_time,
                                 register_no,
                                 detag(pledgor),
                                 detag(pledgee),
                                 state,

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()

                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('equity', i, 5, corp_name, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                announcement_time = \
                                    re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[0]
                                register_no = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[1]
                                pledgor = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[2]
                                pledgee = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[3]
                                state = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[4]
                                # print announcement_time
                                # print register_no
                                # print detag(pledgor)
                                # print detag(pledgee)
                                # print state

                                cursor.execute(
                                    'insert into tyc_equity_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     announcement_time,
                                     register_no,
                                     detag(pledgor),
                                     detag(pledgee),
                                     state,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' - 股权出质信息插入成功  ' + str(datetime.datetime.now())

                else:
                    print u'没有股权出质信息'
                    cursor.execute(
                        'insert into tyc_equity_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
                break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def mortgage(corp_url, conn, cursor, proxy_list):
    print u'爬取动产抵押信息 ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)
            # print html.text            # print html.text
            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'

                continue

            else:

                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name

                if html.text.__contains__('nav-main-mortgageCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-mortgageCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_mortgage > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            # print source
                            register_date = re_findall('<span class=" text-dark-color ">(.*?)</span></td>', source)[
                                0]
                            register_num = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[0]
                            type = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[1]
                            amount = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[2]
                            register_department = \
                                re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[3]

                            state = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[4]

                            # print register_date
                            # print register_num
                            # print type
                            # print amount
                            # print register_department
                            # print state
                            cursor.execute(
                                'insert into tyc_mortgage_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 register_date,
                                 register_num,
                                 type,
                                 amount,
                                 register_department,
                                 state,

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])

                            )
                            conn.commit()

                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('mortgage', i, 5, corp_name, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                # print source
                                register_date = re_findall('<span class=" text-dark-color ">(.*?)</span></td>', source)[
                                    0]
                                register_num = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[0]
                                type = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[1]
                                amount = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[2]
                                register_department = \
                                    re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[3]

                                state = re_findall('<span class=" text-dark-color">(.*?)</span></td>', source)[4]

                                # print register_date
                                # print register_num
                                # print type
                                # print amount
                                # print register_department
                                # print state
                                cursor.execute(
                                    'insert into tyc_mortgage_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     register_date,
                                     register_num,
                                     type,
                                     amount,
                                     register_department,
                                     state,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])

                                )
                                conn.commit()
                    print corp_name + u' - 动产抵押信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print u'没有动产抵押信息'
                    cursor.execute(
                        'insert into tyc_mortgage_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
                break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def towetax(corp_url, conn, cursor, proxy_list):
    print u'爬取欠税公告信息 ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)
            # print html.text
            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'

                continue

            else:

                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name

                if html.text.__contains__('nav-main-ownTaxCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-ownTaxCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_towntax > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            publish_date = \
                                re_findall('<tr><td><span class=".*?text-dark-color.*?">(.*?)</span>', source)[0]
                            taxer_id = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[1]
                            own_tax_type = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[2]
                            tax_amount_now = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[
                                3]
                            tax_balance = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[4]
                            tax_department = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[
                                5]
                            cursor.execute(
                                'insert into tyc_towntax_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 publish_date,
                                 taxer_id,
                                 own_tax_type,
                                 tax_amount_now,
                                 tax_balance,
                                 tax_department,

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('towntax', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                publish_date = \
                                    re_findall('<tr><td><span class=".*?text-dark-color.*?">(.*?)</span>', source)[0]
                                taxer_id = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[1]
                                own_tax_type = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[2]
                                tax_amount_now = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[
                                    3]
                                tax_balance = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[4]
                                tax_department = re_findall('<span class=".*?text-dark-color.*?">(.*?)</span>', source)[
                                    5]

                                # print publish_date
                                # print taxer_id
                                # print own_tax_type
                                # print tax_amount_now
                                # print tax_balance
                                # print tax_department

                                cursor.execute(
                                    'insert into tyc_towntax_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     publish_date,
                                     taxer_id,
                                     own_tax_type,
                                     tax_amount_now,
                                     tax_balance,
                                     tax_department,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' - 欠税公告信息插入成功  ' + str(datetime.datetime.now())

                else:
                    print u'没有欠税公告信息'
                    cursor.execute(
                        'insert into tyc_towntax_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
                break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def judicial(corp_url, conn, cursor, proxy_list):
    print u'爬取司法拍卖信息 ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'

                continue

            else:

                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name

                if html.text.__contains__('nav-main-judicialSaleCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-judicialSaleCount > span')[0].text
                    # print num
                    if int(num) <= 5:
                        res = soup.select('#_container_judicialSale > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            auction_announcement_name = \
                                re_findall('<td><a class="in-block" href=".*?" target="_blank">(.*?)</a></td>',
                                           source)[
                                    0]

                            auction_announcement_url = \
                                re_findall('<td><a class="in-block" href="(.*?)" target="_blank">.*?</a></td>',
                                           source)[
                                    0]
                            date = re_findall('<td><span>(.*?)</span></td>', source)[0]
                            excute_court = \
                                re_findall('<td><span class="text-dark-color in-block">(.*?)</span></td>', source)[
                                    0]
                            auction_target = \
                                re_findall('<td><div class="text-dark-color in-block">(.*?)</div></td>', source)[0]

                            cursor.execute(
                                'insert into tyc_judicial_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 auction_announcement_name,
                                 auction_announcement_url,
                                 date,
                                 excute_court,
                                 detag(auction_target),

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('judicialSale', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                auction_announcement_name = \
                                    re_findall('<td><a class="in-block" href=".*?" target="_blank">(.*?)</a></td>',
                                               source)[
                                        0]

                                auction_announcement_url = \
                                    re_findall('<td><a class="in-block" href="(.*?)" target="_blank">.*?</a></td>',
                                               source)[
                                        0]
                                date = re_findall('<td><span>(.*?)</span></td>', source)[0]
                                excute_court = \
                                    re_findall('<td><span class="text-dark-color in-block">(.*?)</span></td>', source)[
                                        0]
                                auction_target = \
                                    re_findall('<td><div class="text-dark-color in-block">(.*?)</div></td>', source)[0]

                                cursor.execute(
                                    'insert into tyc_judicial_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     auction_announcement_name,
                                     auction_announcement_url,
                                     date,
                                     excute_court,
                                     detag(auction_target),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' - 司法拍卖信息插入成功  ' + str(datetime.datetime.now())

                else:
                    print u'没有司法拍卖信息'
                    cursor.execute(
                        'insert into tyc_judicial_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
                break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def taxCredit(corp_url, conn, cursor, proxy_list):
    print u'爬取税务评级信息 ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'

                continue

            else:

                corp_name = reS_findall('<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name

                if html.text.__contains__('nav-main-taxCreditCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-taxCreditCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_taxcredit > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            year = re_findall('<tr><td><span>(.*?)</span></td>', source)[0]
                            tax_rating = re_findall('<td><span class="">(.*?)</span></td>', source)[0]
                            type = re_findall('<td><span class="">(.*?)</span></td>', source)[1]
                            taxer_id = re_findall('<td><span class="">(.*?)</span></td>', source)[2]
                            evaluation_unit = re_findall('<td><span class="">(.*?)</span></td>', source)[3]

                            cursor.execute(
                                'insert into tyc_taxcredit_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 year,
                                 tax_rating,
                                 type,
                                 taxer_id,
                                 evaluation_unit,

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                    else:

                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('taxcredit', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                year = re_findall('<tr><td><span>(.*?)</span></td>', source)[0]
                                tax_rating = re_findall('<td><span class="">(.*?)</span></td>', source)[0]
                                type = re_findall('<td><span class="">(.*?)</span></td>', source)[1]
                                taxer_id = re_findall('<td><span class="">(.*?)</span></td>', source)[2]
                                evaluation_unit = re_findall('<td><span class="">(.*?)</span></td>', source)[3]

                                cursor.execute(
                                    'insert into tyc_taxcredit_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     year,
                                     tax_rating,
                                     type,
                                     taxer_id,
                                     evaluation_unit,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' - 税务评级信息插入成功  ' + str(datetime.datetime.now())

                else:
                    print u'没有税务评级信息'
                    cursor.execute(
                        'insert into tyc_taxcredit_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
                break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def checkInfo(corp_url, conn, cursor, proxy_list):
    print u'爬取抽查检查信息 ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'

                continue

            else:

                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name

                if html.text.__contains__('nav-main-checkCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-checkCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_check > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])

                            date = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[0]
                            type = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[1]
                            result = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[2]
                            department = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[3]

                            print date
                            print type
                            print result
                            print department

                            cursor.execute(
                                'insert into tyc_check_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 date,
                                 type,
                                 result,
                                 department,

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()

                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_name('check', i, 5, corp_name, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                date = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[0]
                                type = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[1]
                                result = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[2]
                                department = re_findall('<td><span class=".*?">(.*?)</span></td>', source)[3]

                                print date
                                print type
                                print result
                                print department

                                cursor.execute(
                                    'insert into tyc_check_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     date,
                                     type,
                                     result,
                                     department,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' - 抽查检查信息插入成功  ' + str(datetime.datetime.now())

                else:
                    print u'没有抽查检查信息'
                    cursor.execute(
                        'insert into tyc_check_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
                break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def product_info(corp_url, conn, cursor, proxy_list):
    print u'爬取产品信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-productinfo'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-productinfo > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_product > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            image = re_findall('src="(.*?)"', source)
                            part_one = re_findall('<span>(.*?)</span>', source)
                            detail_info = re_findall('<td><script type="text/html">(.*?)</script>', source)
                            # print image[0]
                            # print part_one[0]
                            # print part_one[1]
                            # print part_one[2]
                            # print part_one[3]
                            # print detail_info[0]
                            # print '--------------------'
                            cursor.execute(
                                'insert into tyc_product_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 image[0],
                                 part_one[0].decode('utf-8'),
                                 part_one[1].decode('utf-8'),
                                 part_one[2].decode('utf-8'),
                                 part_one[3].decode('utf-8'),
                                 detag(detail_info[0]).decode('utf-8'),
                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10]
                                 )
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('product', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                image = re_findall('src="(.*?)"', source)
                                part_one = re_findall('<span>(.*?)</span>', source)
                                detail_info = re_findall('<td><script type="text/html">(.*?)</script>', source)
                                # print image[0]
                                # print part_one[0]
                                # print part_one[1]
                                # print part_one[2]
                                # print part_one[3]
                                # print detail_info[0]
                                # print '--------------------'
                                cursor.execute(
                                    'insert into tyc_product_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     image[0],
                                     part_one[0].decode('utf-8'),
                                     part_one[1].decode('utf-8'),
                                     part_one[2].decode('utf-8'),
                                     part_one[3].decode('utf-8'),
                                     detag(detail_info[0]).decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10]
                                     )
                                )
                                conn.commit()
                    print corp_name + u' 产品信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有产品信息 ' + str(datetime.datetime.now())
                    cursor.execute(
                        'insert into tyc_product_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10]
                         )
                    )
                    conn.commit()
            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def certificate(corp_url, conn, cursor, proxy_list):
    print u'爬取资质证书信息 ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'

                continue

            else:

                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name

                if html.text.__contains__('nav-main-certificateCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-certificateCount > span')[0].text
                    # print num
                    if int(num)<=5:
                        res = soup.select('#_container_certificate > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            # print source
                            certificate_type = \
                                re_findall('<td><span class="c9 point hover_underline" onclick=".*?">(.*?)</span>',
                                           source)[0]
                            certificate_num = \
                                re_findall('<span>(.*?)</span>', source)[0]
                            create_time = re_findall('<span>(.*?)</span>', source)[1]
                            invalid_time = re_findall('<span>(.*?)</span>', source)[2]

                            # print certificate_type
                            # print certificate_num
                            # print create_time
                            # print invalid_time

                            cursor.execute(
                                'insert into tyc_certificate_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 certificate_type,
                                 certificate_num,
                                 create_time,
                                 invalid_time,

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('certificate', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                # print source
                                certificate_type = \
                                    re_findall('<td><span class="c9 point hover_underline" onclick=".*?">(.*?)</span>',
                                               source)[0]
                                certificate_num = \
                                    re_findall('<span>(.*?)</span>', source)[0]
                                create_time = re_findall('<span>(.*?)</span>', source)[1]
                                invalid_time = re_findall('<span>(.*?)</span>', source)[2]

                                # print certificate_type
                                # print certificate_num
                                # print create_time
                                # print invalid_time

                                cursor.execute(
                                    'insert into tyc_certificate_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     certificate_type,
                                     certificate_num,
                                     create_time,
                                     invalid_time,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' - 资质证书信息插入成功  ' + str(datetime.datetime.now())

                else:
                    print u'没有资质证书信息'
                    cursor.execute(
                        'insert into tyc_certificate_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
                break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def wechat_info(corp_url, conn, cursor, proxy_list):
    print u'爬取微信公众号信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-weChatCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-weChatCount > span')[0].text

                    if int(num) <= 10 :
                        res = soup.select('#_container_wechat > div.wechat.clearfix > div')
                        for x in range(len(res)):
                            source = str(res[x])
                            wechat_icon = re_findall('src="(.*?)"', source)[0]
                            wechat_name = re_findall('"title":"(.*?)",', source)[0]
                            wechat_num = re_findall('<span class="in-block vertical-top">(.*?)</span>', source)[0]
                            wechat_introduce = re_findall('"recommend":"(.*?)"}</script>', source)[0]
                            QR_code = re_findall('<img alt="code" src="(.*?)" style', source)[0]
                            print wechat_icon
                            print wechat_name
                            print wechat_num
                            print wechat_introduce
                            print QR_code
                            cursor.execute(
                                'insert into tyc_wechat_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,
                                 wechat_icon,
                                 wechat_name,
                                 wechat_num,
                                 wechat_introduce,
                                 QR_code,
                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 10 == 0:
                            all_page_no = int(num) / 10
                        else:
                            all_page_no = int(num) / 10 + 1


                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('wechat', i, 10, corp_id, auth_token)
                            res = soup2.select('div.mb10.in-block.float-left')
                            for x in range(len(res)):
                                source = str(res[x])
                                wechat_icon = re_findall('src="(.*?)"',source)[0]
                                wechat_name = re_findall('"title":"(.*?)",',source)[0]
                                wechat_num = re_findall('<span class="in-block vertical-top">(.*?)</span>',source)[0]
                                wechat_introduce = re_findall('"recommend":"(.*?)"}</script>',source)[0]
                                QR_code = re_findall('<img alt="code" src="(.*?)" style',source)[0]
                                print wechat_icon
                                print wechat_name
                                print wechat_num
                                print wechat_introduce
                                print QR_code
                                cursor.execute(
                                    'insert into tyc_wechat_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,
                                     wechat_icon,
                                     wechat_name,
                                     wechat_num,
                                     wechat_introduce,
                                     QR_code,
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()




                    print corp_name + u' 微信公众号信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有微信公众号信息  ' + str(datetime.datetime.now())

                    cursor.execute(
                        'insert into tyc_wechat_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def iconInfo(corp_url, conn, cursor, proxy_list):
    print u'爬取商标信息 ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'

                continue

            else:

                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name

                if html.text.__contains__('nav-main-tmCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-tmCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_tmInfo > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            register_date = re_findall('<td><span>(.*?)</span></td>', source)[0]
                            icon = re_findall(' src="(.*?)"/></td>', source)[0]
                            icon_name = re_findall('<td><span>(.*?)</span></td>', source)[1]
                            register_num = re_findall('<td><span>(.*?)</span></td>', source)[2]
                            type = re_findall('<td><span>(.*?)</span></td>', source)[3]
                            state = re_findall('<span class="in-block vertical-top">(.*?)</span></td>', source)[0]

                            cursor.execute(
                                'insert into tyc_icon_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 register_date,
                                 icon,
                                 icon_name,
                                 register_num,
                                 type,
                                 state,

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        for i in range(1, int(all_page_no) + 1):
                            print u'爬第' + str(i) + u'页'
                            soup2 = get_cookie_by_id('tmInfo', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                register_date = re_findall('<td><span>(.*?)</span></td>', source)[0]
                                icon = re_findall(' src="(.*?)"/></td>', source)[0]
                                icon_name = re_findall('<td><span>(.*?)</span></td>', source)[1]
                                register_num = re_findall('<td><span>(.*?)</span></td>', source)[2]
                                type = re_findall('<td><span>(.*?)</span></td>', source)[3]
                                state = re_findall('<span class="in-block vertical-top">(.*?)</span></td>', source)[0]

                                cursor.execute(
                                    'insert into tyc_icon_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     register_date,
                                     icon,
                                     icon_name,
                                     register_num,
                                     type,
                                     state,

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' - 商标信息插入成功  ' + str(datetime.datetime.now())

                else:
                    print u'没有商标信息'
                    cursor.execute(
                        'insert into tyc_icon_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
                break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def patent(corp_url, conn, cursor, proxy_list, auth_token):
    print u'爬取专利信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-patentCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-patentCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_patent > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])

                            date = re_findall('<span.*?>(.*?)</span>', source)[0]
                            name = re_findall('<span.*?>(.*?)</span>', source)[1]
                            application_num = re_findall('<span.*?>(.*?)</span>', source)[2]
                            application_for_pubnum = re_findall('<span.*?>(.*?)</span>', source)[3]

                            print date
                            print name
                            print application_num
                            print application_for_pubnum
                            cursor.execute(
                                'insert into tyc_patent_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 date.decode('utf-8'),
                                 name.decode('utf-8'),
                                 application_num.decode('utf-8'),
                                 application_for_pubnum.decode('utf-8'),

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                    else:

                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no

                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('patent', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                date = re_findall('<span.*?>(.*?)</span>', source)[0]
                                name = re_findall('<span.*?>(.*?)</span>', source)[1]
                                application_num = re_findall('<span.*?>(.*?)</span>', source)[2]
                                application_for_pubnum = re_findall('<span.*?>(.*?)</span>', source)[3]

                                print date
                                print name
                                print application_num
                                print application_for_pubnum
                                cursor.execute(
                                    'insert into tyc_patent_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     date.decode('utf-8'),
                                     name.decode('utf-8'),
                                     application_num.decode('utf-8'),
                                     application_for_pubnum.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' 专利信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有专利信息  ' + str(datetime.datetime.now())

                    cursor.execute(
                        'insert into tyc_patent_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
            break

        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def copyR(corp_url, conn, cursor, proxy_list):
    print u'获取软件著作权部分  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)
            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-cpoyRCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-cpoyRCount > span')[0].text
                    if int(num) <= 5:
                        res = soup.select('#_container_copyright > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])

                            obj = re_findall('<span>(.*?)</span>', source)
                            date = obj[0]
                            name = obj[1]
                            brief_name = obj[2]
                            registration_no = obj[3]
                            classification_no = obj[4]
                            version_no = obj[5]

                            # print date
                            # print name
                            # print brief_name
                            # print registration_no
                            # print classification_no
                            # print version_no
                            cursor.execute(
                                'insert into tyc_copyRcount_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s" )' %
                                (corp_url,
                                 corp_name,
                                 corp_id,
                                 date.decode('utf-8'),
                                 name.decode('utf-8'),
                                 brief_name.decode('utf-8'),
                                 registration_no.decode('utf-8'),
                                 classification_no.decode('utf-8'),
                                 version_no.decode('utf-8'),

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()

                    else:
                        auth_token = get_auth_token()

                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('copyright', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                obj = re_findall('<span>(.*?)</span>', source)
                                date = obj[0]
                                name = obj[1]
                                brief_name = obj[2]
                                registration_no = obj[3]
                                classification_no = obj[4]
                                version_no = obj[5]

                                # print date
                                # print name
                                # print brief_name
                                # print registration_no
                                # print classification_no
                                # print version_no
                                cursor.execute(
                                    'insert into tyc_copyRcount_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s" )' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,
                                     date.decode('utf-8'),
                                     name.decode('utf-8'),
                                     brief_name.decode('utf-8'),
                                     registration_no.decode('utf-8'),
                                     classification_no.decode('utf-8'),
                                     version_no.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' 软件著作权信息  插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有软件著作权信息  ' + str(datetime.datetime.now())

                    cursor.execute(
                        'insert into tyc_copyRcount_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s" )' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()
            break

        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def copyrightWorks(corp_url, conn, cursor, proxy_list):
    print u'获取作品著作权部分  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name

                if html.text.__contains__('nav-main-copyrightWorks'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-copyrightWorks > span')[0].text
                    if int(num)<=5:
                        res = soup.select('#_container_copyrightWorks > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])

                            obj = re_findall('<span>(.*?)</span>', source)
                            name = obj[0]
                            register_no = obj[1]
                            category = obj[2]
                            finish_date = obj[3]
                            register_date = obj[4]
                            publish_date = obj[5]

                            # print name
                            # print register_no
                            # print category
                            # print finish_date
                            # print register_date
                            # print publish_date
                            cursor.execute(
                                'insert into tyc_copyRworks_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,

                                 name.decode('utf-8'),
                                 register_no.decode('utf-8'),
                                 category.decode('utf-8'),
                                 finish_date.decode('utf-8'),
                                 register_date.decode('utf-8'),
                                 publish_date.decode('utf-8'),

                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])

                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('copyrightWorks', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])

                                obj = re_findall('<span>(.*?)</span>', source)
                                name = obj[0]
                                register_no = obj[1]
                                category = obj[2]
                                finish_date = obj[3]
                                register_date = obj[4]
                                publish_date = obj[5]

                                # print name
                                # print register_no
                                # print category
                                # print finish_date
                                # print register_date
                                # print publish_date
                                cursor.execute(
                                    'insert into tyc_copyRworks_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,

                                     name.decode('utf-8'),
                                     register_no.decode('utf-8'),
                                     category.decode('utf-8'),
                                     finish_date.decode('utf-8'),
                                     register_date.decode('utf-8'),
                                     publish_date.decode('utf-8'),

                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])

                                )
                                conn.commit()
                    print corp_name + u' 作品著作权信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有作品著作权信息 ' + str(datetime.datetime.now())

                    cursor.execute(
                        'insert into tyc_copyRworks_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])

                    )
                    conn.commit()
            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break


def website_record(corp_url, conn, cursor, proxy_list):
    print u'爬取网站备案信息  ' + str(datetime.datetime.now())
    corp_id = corp_url.split('/company/')[1]

    while True:
        try:
            index = random.randint(0, len(proxy_list) - 1)
            current_proxy = proxy_list[index]
            print "NEW PROXY:\t%s" % current_proxy
            proxies = {"http": "http://" + current_proxy, "https": "http://" + current_proxy, }

            headers = {
                'Cookie': 'TYCID=8c420960894b11e79bb7cf4adc554d53; uccid=baeee58fe4d1d697092e61f6525e8719; ssuid=6805162414; aliyungf_tc=AQAAAOsOUQId4QcAlaRf3mqAPMUDMG/2; csrfToken=S2nttCpDrr4WCbvLkQRClEUt; bannerFlag=true; _csrf=i6MDX6NEr+KEpAxRAcWeaA==; OA=cxAohDKsDZv4yk4sQ70GtLb5KtPEhEnIp/d25AgGeuU=; _csrf_bk=76b9aab25bdab0db8930d22ee4171984; Hm_lvt_e92c8d65d92d534b0fc290df538b4758=1503634325,1504143041,1504148840,1504245847; Hm_lpvt_e92c8d65d92d534b0fc290df538b4758=1504490343',
                'Host': 'www.tianyancha.com',
                'Referer': 'https://www.tianyancha.com/',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla / 5.0(Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
            html = requests.get(corp_url, proxies=proxies, timeout=10, headers=headers)

            if html.text.__contains__('天眼查-人人都在用商业安全工具'):
                print 'parse error , retry'
                continue
            else:
                corp_name = \
                    reS_findall(
                        '<title>(.*?)</title>', str(html.text))[0].split('_')[0]

                print corp_name
                if html.text.__contains__('nav-main-icpCount'):
                    soup = BeautifulSoup(html.text, 'lxml')
                    num = soup.select('#nav-main-icpCount > span')[0].text
                    if int(num) <=5:
                        res = soup.select('#_container_icp > div > div > table > tbody > tr')
                        for x in range(len(res)):
                            source = str(res[x])
                            part_one = re_findall('<td><span>(.*?)</span></td>', source)
                            part_two = re_findall('target="_blank">(.*?)</a></td>', source)
                            check_time = part_one[0]
                            website_name = part_one[1]
                            homepage = part_two[0]
                            domain = re_findall('<td>(.*?)</td>', source)[3]
                            record_number = part_one[2]
                            state = part_one[3]
                            unit_character = part_one[4]

                            cursor.execute(
                                'insert into tyc_webrecord_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                (corp_url,
                                 corp_name,
                                 corp_id,
                                 check_time.decode('utf-8'),
                                 website_name.decode('utf-8'),
                                 homepage.decode('utf-8'),
                                 domain.decode('utf-8'),
                                 record_number.decode('utf-8'),
                                 state.decode('utf-8'),
                                 unit_character.decode('utf-8'),
                                 str(datetime.datetime.now()),
                                 str(datetime.datetime.now())[:10])
                            )
                            conn.commit()
                    else:
                        auth_token = get_auth_token()
                        if int(num) % 5 == 0:
                            all_page_no = int(num) / 5
                        else:
                            all_page_no = int(num) / 5 + 1
                        print all_page_no
                        for i in range(1, int(all_page_no) + 1):
                            soup2 = get_cookie_by_id('icp', i, 5, corp_id, auth_token)
                            res = soup2.select('tr')
                            for x in range(1, len(res)):
                                source = str(res[x])
                                part_one = re_findall('<td><span>(.*?)</span></td>', source)
                                part_two = re_findall('target="_blank">(.*?)</a></td>', source)
                                check_time = part_one[0]
                                website_name = part_one[1]
                                homepage = part_two[0]
                                domain = re_findall('<td>(.*?)</td>', source)[3]
                                record_number = part_one[2]
                                state = part_one[3]
                                unit_character = part_one[4]

                                cursor.execute(
                                    'insert into tyc_webrecord_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                                    (corp_url,
                                     corp_name,
                                     corp_id,
                                     check_time.decode('utf-8'),
                                     website_name.decode('utf-8'),
                                     homepage.decode('utf-8'),
                                     domain.decode('utf-8'),
                                     record_number.decode('utf-8'),
                                     state.decode('utf-8'),
                                     unit_character.decode('utf-8'),
                                     str(datetime.datetime.now()),
                                     str(datetime.datetime.now())[:10])
                                )
                                conn.commit()
                    print corp_name + u' 微信公众号信息插入成功  ' + str(datetime.datetime.now())
                else:
                    print corp_name + u' 没有微信公众号信息  ' + str(datetime.datetime.now())

                    cursor.execute(
                        'insert into tyc_webrecord_info values ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")' %
                        (corp_url,
                         corp_name,
                         corp_id,

                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',
                         'no_data',

                         str(datetime.datetime.now()),
                         str(datetime.datetime.now())[:10])
                    )
                    conn.commit()

            break
        except Exception, e:
            if str(e).find('HTTPSConnectionPool') >= 0:
                print 'HTTPSConnectionPool , retry '
                continue
            elif str(e).find("OperationalError: (2006, 'MySQL server has gone away')") >= 0:
                return 'MySql error 2006 ,restart'
            elif str(e).find('2003') >= 0:
                return 'MySql error 2003 ,restart'
            elif str(e).find('Connection aborted') >= 0:
                print 'Connection aborted occured~~~!!!'
                continue
            else:
                print corp_url + ' unknown error with info ' + str(e)
                print traceback.format_exc()
                break

                # ===================================================
                # def get_Info(corp_url, conn, cursor, proxy_list):
                #     get_base_business_info(corp_url, conn, cursor, proxy_list)
                # conn.commit()


def get_InfoWorker(pairs, proxy_list):
    try:

        conn = MySQLdb.connect(host=db_config.server09_host, port=db_config.server09_port, user=db_config.server09_user,
                               passwd=db_config.server09_passwd,
                               db=db_config.server09_dbname,
                               charset="utf8")
        cursor = conn.cursor()

        for pair in pairs:
            print '1223'
            # get_base_business_info(pair[0], conn, cursor, proxy_list)
            # staff(pair[0], conn, cursor, proxy_list)
            # shareholder(pair[0], conn, cursor, proxy_list)
            # out_invest(pair[0], conn, cursor, proxy_list)

            branch(pair[0], conn, cursor, proxy_list)
            core_team(pair[0], conn, cursor, proxy_list)
            abnormal(pair[0], conn, cursor, proxy_list)
            illegal(pair[0], conn, cursor, proxy_list)
            equity(pair[0], conn, cursor, proxy_list)
            mortgage(pair[0], conn, cursor, proxy_list)
            towetax(pair[0], conn, cursor, proxy_list)
            taxCredit(pair[0], conn, cursor, proxy_list)
            certificate(pair[0], conn, cursor, proxy_list)
            iconInfo(pair[0], conn, cursor, proxy_list)
            checkInfo(pair[0], conn, cursor, proxy_list)
            judicial(pair[0], conn, cursor, proxy_list)
            cursor.execute('insert into tyc_log_info values ("%s","%s","%s")' %
                           (
                               pair[0],
                               str(datetime.datetime.now()),
                               str(datetime.datetime.now())[:10]
                           )

                           )
            conn.commit()
            print pair[0] + u' 的信息都采集了 ,录入log表  ' + str(datetime.datetime.now())

        cursor.close()
        conn.close()
    except:
        print traceback.format_exc()


if __name__ == '__main__':

    conn = MySQLdb.connect(host=db_config.server09_host,
                           port=db_config.server09_port,
                           user=db_config.server09_user,
                           passwd=db_config.server09_passwd,
                           db=db_config.server09_dbname,
                           charset="utf8")

    cursor = conn.cursor()

    p = Pool()
    pNum = 4
    span = 5

    # cursor.execute(
    #     "select distinct a.corp_url from tyc_prepare_info a left join tyc_base_business_info b "
    #     "on a.corp_url=b.corp_url where b.corp_url is null limit " + str(
    #         pNum * span))
    cursor.execute(
        "select distinct a.corp_url from tyc_prepare_info a left join tyc_log_info b on a.corp_url=b.corp_url where b.corp_url is null limit  " + str(
            pNum * span))

    pairs = cursor.fetchall()
    print '%s records found!' % (len(pairs))

    cursor.close()
    conn.close()

    # auth_token = get_auth_token()

    for i in range(pNum):
        # p.apply_async(searchWorker, (pairs[i * span:(i + 1) * span], proxy_list,))  # 增加新的进程
        p.apply_async(get_InfoWorker, (pairs[i * span:(i + 1) * span], proxy_list))  # 增加新的进程

    p.close()  # 禁止在增加新的进程
    p.join()
    print "pool process done"
    quit()
# if __name__ == '__main__':
#     conn = MySQLdb.connect(host=db_config.server09_host, port=db_config.server09_port, user=db_config.server09_user,
#                            passwd=db_config.server09_passwd,
#                            db=db_config.server09_dbname,
#                            charset="utf8")
#     cursor = conn.cursor()
#
#     url = 'https://www.tianyancha.com/company/141796533'
#     branch(url, conn, cursor, proxy_list)
